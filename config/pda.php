<?php

return [
    'submission' => [
        // Default image to use when a submission does not have a profile image set
        // This image should be in storage/images, and excluded from the ignore.
        'profile_not_set' => 'no_profile_image.png',
    ],
];

<?php

namespace Pda\Entities;

use Pda\Entities\User;
use Pda\Entities\Submission;
use Pda\Entities\AssessmentGroup;
use Illuminate\Database\Eloquent\Model;
use Pda\Entities\Contracts\AssessmentDefinition;

/**
 * Assessment model
 *
 * @package    Pda
 * @subpackage Entities
*/
class Assessment extends Model implements AssessmentDefinition
{
    protected $fillable = [
        'submission_files',
        'achievements',
        'current_engagements',
        'ambitions_plan_of_action',
        'apra_career_advancement',
    ];

    /**
     * An assessment belongs to an assessment group.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function group()
    {
        return $this->belongsTo(AssessmentGroup::class, 'assessment_group_id');
    }


    /**
     * An assessment belongs to an assessor (user).
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assessor()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * An assessment belongs to a submission.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function submission()
    {
        return $this->belongsTo(Submission::class);
    }


    /**
     * Accessor for the maximum score an assessment can obtain.
     *
     * @return int
     */
    public function getMaximumScoreAttribute()
    {
        return static::MAXIMUM_SCORE;
    }


    /**
     * Get the total score for this assessment record.
     *
     * @return int
     */
    public function getTotalScoreAttribute()
    {
        return $this->getTotalScore();
    }


    /**
     * Get the average score for this assessment record.
     *
     * @return float
     */
    public function getAverageScoreAttribute()
    {
        return $this->getTotalScore() / count($this->getScoreFields());
    }


    /**
     * Get the total score for this assessment record.
     *
     * @return integer
     */
    private function getTotalScore()
    {
        return array_sum(array_intersect_key($this->attributes, array_flip($this->getScoreFields())));
    }


    /**
     * Return the database fields used to calculate the score for this assessment record.
     *
     * @return array
     */
    public static function getScoreFields()
    {
        return [
            'submission_files',
            'achievements',
            'current_engagements',
            'ambitions_plan_of_action',
            'apra_career_advancement',
        ];
    }
}

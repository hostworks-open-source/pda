<?php

namespace Pda\Entities;

use Cache;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Pda\Entities\Assessment;
use Pda\Entities\Contracts\CreatableUser;
use Pda\Entities\Panel;
use Pda\Entities\Partner;
use Pda\Entities\Role;
use Pda\Entities\Submission;
use Pda\Entities\Traits\CreatesUsers;
use Pda\Entities\Verification;

/**
 * User model
 *
 * @package    Pda
 * @subpackage Entities
*/
class User extends Model implements AuthenticatableContract, CanResetPasswordContract, CreatableUser
{
    use Authenticatable, CanResetPassword, CreatesUsers;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'name_first', 'name_last', 'email', 'password', 'role_id', ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [ 'password', 'remember_token' ];


    /**
     * A user belongs to many roles.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }


    /**
     * A user has one submission.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function submission()
    {
        return $this->hasOne(Submission::class);
    }


    /**
     * A user has many verifications.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function verifications()
    {
        return $this->hasMany(Verification::class);
    }


    /**
     * A user has many assessments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assessments()
    {
        return $this->hasMany(Assessment::class);
    }


    /**
     * A user belongs to one partner organisation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }


    /**
     * A user belongs to many panels.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function panels()
    {
        return $this->belongsToMany(Panel::class);
    }


    /**
     * Determine whether the given user is flagged as a superuser.
     *
     * @return boolean
     */
    public function isSuperUser()
    {
        return $this->superuser === 1;
    }


    /**
     * Determine if the user has any of the given role(s).
     * Superuser will always returns true for any check.
     *
     * This function caches the results, keyed on the user id, in addition
     * to the roles being checked on. This aims to prevent many queries
     * against the db several times per page load, as we often check
     * the user's roles for various different sections of the page.
     *
     * There is currently no way to manage a user's roles from within the
     * application itself, so we don't worry about user roles changing.
     *
     * @param  array|string $roles A single array of multiple roles to check,
     *                             or multiple args, which will be used in the same way.
     *
     * @return bool
     */
    public function hasRole($roles)
    {
        if (is_string($roles)) {
            $roles = func_get_args();
        }

        $user     = $this;
        $cacheKey = sprintf('userRoles.%d.%s', $user->id, join('', $roles));
        $hasRole  = Cache::remember($cacheKey, 5, function () use ($user, $roles) {
            return $user->roles()->whereIn('name', $roles)->count() > 0;
        });

        return $this->isSuperUser() || $hasRole;
    }


    /**
     * Return the user full name.
     *
     * @return string
     */
    public function getNameAttribute()
    {
        return sprintf('%s %s', $this->attributes['name_first'], $this->attributes['name_last']);
    }


    /**
     * Ensure the password is enrypted when being set.
     *
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }


    /**
     * Scope to find all users of the given role type.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query  Query builder instance
     * @param  int $roleId Role identifier to return users for
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfType($query, $roleId)
    {
        return $query->whereHas('roles', function ($query) use ($roleId) {
            return $query->where('role_id', $roleId);
        });
    }
}

<?php

namespace Pda\Entities;

use GlideImage;
use Hashids;
use Iatstuti\Database\Support\NullableFields;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Laracasts\Presenter\PresentableTrait;
use Pda\Entities\Assessment;
use Pda\Entities\Category;
use Pda\Entities\Contracts\SubmissionDefinition;
use Pda\Entities\Panel;
use Pda\Entities\Partner;
use Pda\Entities\Status;
use Pda\Entities\SubmissionFile;
use Pda\Entities\Traits\SubmissionSectionsComplete;
use Pda\Entities\User;
use Pda\Entities\Verification;
use Pda\IdentifiesSubmissionFileRequirements;

/**
 * Submission model
 *
 * @package    Pda
 * @subpackage Entities
*/
class Submission extends Model implements SubmissionDefinition
{
    use NullableFields, PresentableTrait, SubmissionSectionsComplete, IdentifiesSubmissionFileRequirements;

    protected $presenter = 'Pda\Presenters\SubmissionPresenter';

    protected $fillable = [
        'category_id',
        'partner_id',
        'partner_organisation_rank',
        'round_one_rank',
        'round_two_rank',
        'performer_name',
        'address_1',
        'address_2',
        'city',
        'state',
        'postcode',
        'country',
        'phone_number',
        'proof_of_identity',
        'profile_image',
        'biography',
        'achievements',
        'current_engagements',
        'ambitions_plan_of_action',
        'apra_career_advancement',
        'submitted_at',
    ];

    protected $nullable = [
        'user_id',
        'category_id',
        'partner_id',
        'performer_name',
        'address_1',
        'address_2',
        'city',
        'state',
        'postcode',
        'country',
        'phone_number',
        'profile_image',
        'proof_of_identity',
        'biography',
        'achievements',
        'current_engagements',
        'ambitions_plan_of_action',
        'apra_career_advancement',
        'submitted_at',
    ];

    protected $dates = [ 'created_at', 'updated_at', 'submitted_at', ];


    /**
     * Overload the boot method so that we can tackle setting the progress percentage automagically.
     */
    protected static function boot()
    {
        static::saving(function ($submission) {
            $progress_fields = [
                'category_id'              => 5,
                'partner_id'               => 5,
                'performer_name'           => 5,
                'address_1'                => 5,
                'city'                     => 5,
                'state'                    => 5,
                'postcode'                 => 5,
                'country'                  => 5,
                'phone_number'             => 5,
                'profile_image'            => 5,
                'proof_of_identity'        => 5,
                'biography'                => 5,
                'achievements'             => 5,
                'current_engagements'      => 5,
                'ambitions_plan_of_action' => 5,
                'apra_career_advancement'  => 5,
            ];

            $submission->progress_percent = min(
                100,
                array_reduce(
                    array_keys($submission->getAttributes()),
                    function ($carry, $key) use ($progress_fields, $submission) {
                        return array_key_exists($key, $progress_fields) && trim($submission->getAttribute($key)) !== ''
                            ? $carry + $progress_fields[$key]
                            : $carry;
                    },
                    0
                )
            );

            // Ensure that the submission has a user - *should* be true always - and that first
            // and last name are not empty. Don't just assume that these values aren't empty.
            if ($submission->user && trim($submission->user->name_first) !== '' && trim($submission->user->name_last) !== '') {
                $submission->progress_percent += 5;
            }

            // If the user has any files, including having non-empty title and
            // description, that counts as 5% towards profile completion.
            if ($submission->workComplete) {
                $submission->progress_percent += 5;
            }

            // A profile's progress percentage starts at this value. It should be the difference
            // between the sum of the individual fields and the maximum value achievable (100)
            $submission->progress_percent += 10;
        });

        parent::boot();
    }


    /**
     * A submission belongs to a user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * A submission belongs to a category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }


    /**
     * A submission belongs to a partner.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function partner()
    {
        return $this->belongsTo(Partner::class);
    }


    /**
     * A submission has many files.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(SubmissionFile::class)->oldest();
    }


    /**
     * A submission belongs to a status.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(Status::class);
    }


    /**
     * A submission has many verifications.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function verifications()
    {
        return $this->hasMany(Verification::class);
    }


    /**
     * Return only the latest verification.
     *
     * @return mixed
     */
    public function latestVerification()
    {
        return $this->hasOne(Verification::class)->latest();
    }


    /**
     * A submission has many assessments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assessments()
    {
        return $this->hasMany(Assessment::class);
    }


    /**
     * A submission has one assessment from a given user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function userAssessment()
    {
        return $this->hasOne(Assessment::class)->where('user_id', auth()->id());
    }


    /**
     * A submission belongs to many panels.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function panels()
    {
        return $this->belongsToMany(Panel::class);
    }


    /**
     * Return the average assessment score for the given round for this submission.
     *
     * @param  Assessment $assessment Assessment group to get results for
     * @return int
     */
    public function getRoundAssessmentAverage(AssessmentGroup $assessmentGroup)
    {
        $result = $this->assessments()
            ->select(\DB::raw(sprintf('AVG (%s) as `roundAverage`', join(' + ', Assessment::getScoreFields()))))
            ->where('assessment_group_id', $assessmentGroup->id)
            ->groupBy('submission_id')
            ->first();

        return $result ? (int) round($result->roundAverage) : 0;
    }


    /**
     * Set the biography attribute.
     *
     * @param  $biography
     */
    public function setBiographyAttribute($biography)
    {
        $this->attributes['biography'] = strip_tags($biography);
    }


    /**
     * Set the achievements attribute.
     *
     * @param  $achievements
     */
    public function setAchievementsAttribute($achievements)
    {
        $this->attributes['achievements'] = strip_tags($achievements);
    }


    /**
     * Set the current engagements attribute.
     *
     * @param  $current_engagements
     */
    public function setCurrentEngagementsAttribute($current_engagements)
    {
        $this->attributes['current_engagements'] = strip_tags($current_engagements);
    }


    /**
     * Set the ambitions and plan of action attribute.
     *
     * @param  $ambitions_plan_of_action
     */
    public function setAmbitionsPlanOfActionAttribute($ambitions_plan_of_action)
    {
        $this->attributes['ambitions_plan_of_action'] = strip_tags($ambitions_plan_of_action);
    }


    /**
     * Set the APRA career advancement attribute.
     *
     * @param  $apra_career_advancement
     */
    public function setApraCareerAdvancementAttribute($apra_career_advancement)
    {
        $this->attributes['apra_career_advancement'] = strip_tags($apra_career_advancement);
    }


    /**
     * As we want to add a 'Select partner organisation' option to the top of a dropdown
     * list in the frontend, we must number the value with a zero, however, 0 breaks
     * the database FK constraint, so if we receive a  zero value, we should set
     * the attribute value to null.
     *
     * @param $partner_id
     */
    public function setPartnerIdAttribute($partner_id)
    {
        $this->attributes['partner_id'] = $partner_id == 0 ? null : $partner_id;
    }


    /**
     * Get this submission's hash - the encoded id.
     *
     * @return mixed
     */
    public function getHashAttribute()
    {
        return Hashids::encode($this->attributes['id']);
    }


    /**
     * Determine whether or not the submission has been verified.
     *
     * @return bool
     */
    public function getVerifiedAttribute()
    {
        return in_array($this->attributes['status_id'], [ Status::APPROVED, Status::REJECTED, ]);
    }


    /**
     * Determine whether or not the submission is still pending.
     *
     * @return boolean
     */
    public function getIsPendingAttribute()
    {
        return $this->attributes['status_id'] == Status::PENDING;
    }


    /**
     * Get a link to the submission's profile image.
     *
     * @see http://glide.thephpleague.com/api/size/ For a list of parameters you can pass
     *
     * @param array $parameters Modify the image on the fly
     *
     * @return string
     */
    public function profileImageLink(array $parameters = [ ])
    {
        $profile_image = $this->attributes['profile_image'] ?: config('pda.submission.profile_not_set');

        $parameters = array_merge([
            'h'   => 180,
            'w'   => 180,
            'fit' => 'crop',
        ], $parameters);

        return GlideImage::load($profile_image, $parameters)->getURL();
    }


    /**
     * Get a link to the submissions proof of identity image.
     *
     * @param array $parameters
     *
     * @return null|string
     */
    public function proofOfIdentityLink(array $parameters = [ ])
    {
        if (! $this->attributes['proof_of_identity']) {
            return null;
        }

        $parameters = array_merge([
            'w'   => 600,
            'fit' => 'resize',
        ], $parameters);

        return GlideImage::load($this->attributes['proof_of_identity'], $parameters)->getURL();
    }


    /**
     * Filter submissions by status.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  \Illuminate\Http\Request              $request
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeFilter($query, Request $request)
    {
        if ($request->has('status_id') && $request->get('status_id')) {
            $query->where('status_id', $request->get('status_id'));
        }

        if ($request->has('category_id') && $request->get('category_id')) {
            $query->where('category_id', $request->get('category_id'));
        }

        if ($request->has('partner_id') && $request->get('partner_id')) {
            $query->where('partner_id', $request->get('partner_id'));
        }

        if ($request->has('name') || $request->has('email')) {
            // Ensure that we grab all fields from the submissions table only,
            // in order to prevent collisions with the id field, which is
            // then used for eager loading other model relationships.
            $query->join('users', 'users.id', '=', 'submissions.user_id')->select('submissions.*');

            if (trim($request->get('name')) !== '') {
                $query->where(function ($query) use ($request) {
                    $query->where(\DB::raw('CONCAT_WS(" ", users.name_first, users.name_last)'), 'LIKE', '%' . $request->get('name') . '%')
                          ->orWhere('submissions.performer_name', 'LIKE', '%' . $request->get('name') . '%');
                });
            } elseif (trim($request->get('email')) !== '') {
                $query->where('users.email', $request->get('email'));
            }
        }

        return $query;
    }


    /**
     * Query scope to filter submissions that can be assigned to the given judging panel.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query Eloquent query builder instance
     * @param  Panel                                 $panel Panel model instance
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCanBeAssignedToPanel($query, Panel $panel)
    {
        return $query->where('status_id', Status::APPROVED)
                     ->where('progress_percent', 100)
                     ->where($panel->eligibility_ranking, '>', 0)
                     ->whereDoesntHave('panels', function ($query) use ($panel) {
                        $query->where('assessment_group_id', $panel->assessment_group_id);
                     })
                     ->orderBy('partner_organisation_rank', 'desc');
    }


    /**
     * Query scope to return only applications that have not
     * already been assessed by the logged in user.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query             Builder instance
     * @param  int                                   $assessmentGroupId Assessment group identifier
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAssessable($query, $assessmentGroupId)
    {
        return $query->whereDoesntHave('userAssessment', function ($query) use ($assessmentGroupId) {
            $query->where('assessment_group_id', $assessmentGroupId);
        });
    }


    /**
     * Query scope to return only applications that have
     * already been assessed by the logged in user.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  int $assessmentGroupId
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAssessed($query, $assessmentGroupId)
    {
        return $query->whereHas('userAssessment', function ($query) use ($assessmentGroupId) {
            $query->where('assessment_group_id', $assessmentGroupId);
        });
    }


    /**
     * Scope to sort results by.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  \Illuminate\Http\Request               $request
     * @return void
     */
    public function scopeSort($query, Request $request)
    {
        $direction = $request->get('order') ?: 'asc';
        $column    = $request->get('sort');

        if (! is_null($column) && $this->isAllowedSort($column)) {
            if (in_array($column, [ 'name', 'email', 'category', ])) {
                $query->select('submissions.*');
            }

            switch ($column) {
                case 'name':
                    $query->join('users', 'users.id', '=', 'submissions.user_id')->orderBy(\DB::raw('CONCAT_WS(" ", users.name_first, users.name_last)'), $direction);
                    break;
                case 'email':
                    $query->join('users', 'users.id', '=', 'submissions.user_id')->orderBy('users.email', $direction);
                    break;
                case 'category':
                    $query->join('categories', 'categories.id', '=', 'submissions.category_id')->orderBy('categories.name', $direction);
                    break;
                case 'performer_name':
                case 'submitted_at':
                case 'partner_organisation_rank':
                case 'round_one_rank':
                case 'round_two_rank':
                    $query->orderBy(sprintf('submissions.%s', $column), $direction);
                    break;
            }
        }

        return $query;
    }


    /**
     * Determine whether the given column is allowed to be sorted.
     *
     * @param  string  $column The column to check
     * @return boolean
     */
    private function isAllowedSort($column)
    {
        return in_array($column, [
            'name',
            'performer_name',
            'email',
            'category',
            'submitted_at',
            'partner_organisation_rank',
            'round_one_rank',
            'round_two_rank',
        ]);
    }
}

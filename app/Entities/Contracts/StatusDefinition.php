<?php

namespace Pda\Entities\Contracts;

/**
 * Status model definition contract
 *
 * @package    Pda
 * @subpackage Entities
*/
interface StatusDefinition
{
    /**
     * Pending status
     */
    const PENDING = 1;

    /**
     * Submitted status
     */
    const SUBMITTED = 2;

    /**
     * Approved status
     */
    const APPROVED = 3;

    /**
     * Rejected status
     */
    const REJECTED = 4;
}

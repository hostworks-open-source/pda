<?php

namespace Pda\Entities\Contracts;

/**
 * Category model definition contract
 *
 * @package    Pda
 * @subpackage Entities\Contracts
*/
interface CategoryDefinition
{
    /**#@+
     * Category constant
     */
    /** Popular contemporary */
    const POPULAR_CONTEMPORARY = 1;

    /** Classical */
    const CLASSICAL = 2;

    /** Jazz */
    const JAZZ = 3;

    /** Aboriginal & Torres Strait Islander (ATSI) */
    const ATSI = 4;

    /** Film & Television */
    const FILM_AND_TELEVISION = 5;

    /** Country */
    const COUNTRY = 6;
    /**#@-*/
}

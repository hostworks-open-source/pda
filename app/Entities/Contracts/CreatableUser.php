<?php

namespace Pda\Entities\Contracts;

/**
 * Define the various user type creation methods
 *
 * @package    Pda
 * @subpackage Entities\Contracts
*/
interface CreatableUser
{
    /**#@+
     * @param  array  $attributes Attributes with which to create the user
     * @return Pda\Entities\User
     */
    public static function createAdmin(array $attributes = [ ]);

    /** Create a partner user. */
    public static function createPartner(array $attributes = [ ]);

    /** Create a staff user. */
    public static function createStaff(array $attributes = [ ]);

    /** Create a judge user */
    public static function createJudge(array $attributes = [ ]);

    /** Create an applicant user */
    public static function createApplicant(array $attributes = [ ]);

    /**
     * Create a user of the given role.
     *
     * @param  int    $role_id    Identifier for the user role
     * @param  array  $attributes Attributes with which to create the user
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public static function createUserOfType($role_id, array $attributes = [ ]);
    /**#@-*/
}

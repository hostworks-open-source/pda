<?php

namespace Pda\Entities\Contracts;

/**
 * Role definition contract
 *
 * @package    Pda
 * @subpackage Entities\Contracts
*/
interface RoleDefinition
{
    /**#@+
     * Role constant
     */
    /** Admin */
    const ADMIN = 1;

    /** Partner */
    const PARTNER = 2;

    /** Staff */
    const STAFF = 3;

    /** Judge */
    const JUDGE = 4;

    /** Applicant */
    const APPLICANT = 5;
    /**#@-*/
}

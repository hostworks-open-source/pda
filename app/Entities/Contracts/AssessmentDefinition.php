<?php

namespace Pda\Entities\Contracts;

/**
 * Assessment model definition contract
 *
 * @package    Pda
 * @subpackage Entities\Contracts
*/
interface AssessmentDefinition
{
    /** The maximum score an assessment can obtain */
    const MAXIMUM_SCORE = 100;
}

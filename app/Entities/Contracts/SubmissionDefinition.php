<?php

namespace Pda\Entities\Contracts;

/**
 * Submission definition interface
 *
 * @package    Pda
 * @subpackage Entities\Contracts
*/
interface SubmissionDefinition
{
    /**#@+
     * Submission content section
     */
    /**
     * Details section
     */
    const SECTION_DETAILS = 'details';

    /**
     * Answers section
     */
    const SECTION_ANSWERS = 'answers';
    /**#@-*/
}

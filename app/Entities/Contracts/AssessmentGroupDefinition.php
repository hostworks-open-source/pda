<?php

namespace Pda\Entities\Contracts;

/**
 * Assessment group definition contract
 *
 * @package    Pda
 * @subpackage Entities\Contracts
*/
interface AssessmentGroupDefinition
{
    /**#@+
     * Assessment group identifier
     */
    /** Partner */
    const PARTNER = 1;

    /** Round 1 */
    const ROUND_ONE = 2;

    /** Round 2 */
    const ROUND_TWO = 3;
    /**#@-*/
}

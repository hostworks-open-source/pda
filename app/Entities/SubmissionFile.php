<?php

namespace Pda\Entities;

use Iatstuti\Database\Support\NullableFields;
use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;
use Pda\Entities\Submission;
use Symfony\Component\HttpFoundation\File\MimeType\ExtensionGuesser;

/**
 * Submission File model
 *
 * @package    Pda
 * @subpackage Entities
*/
class SubmissionFile extends Model
{
    use NullableFields, PresentableTrait;

    protected $presenter = 'Pda\Presenters\SubmissionFilePresenter';

    protected $fillable = [ 'filename', 'title', 'description', 'filesize', 'mime_type', 'file_group', ];

    protected $nullable = [ 'submission_id', 'title', 'description', 'filesize', 'mime_type', 'file_group', ];

    protected $casts = [
        'filesize' => 'int',
    ];


    /**
     * A submission file belongs to a submission.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function submission()
    {
        return $this->belongsTo(Submission::class);
    }


    /**
     * Strip tags from the description attribute.
     *
     * @param $description
     */
    public function setDescriptionAttribute($description)
    {
        $this->attributes['description'] = strip_tags($description);
    }


    /**
     * Get the storage path for this file.
     *
     * @return string
     */
    public function getPathAttribute()
    {
        return storage_path(sprintf('applications/%s/%s', $this->submission->hash, $this->filename));
    }


    /**
     * Determine if this submission file is an image.
     *
     * @return boolean
     */
    public function isImage()
    {
        $guesser = ExtensionGuesser::getInstance();

        return in_array($guesser->guess($this->mime_type), [ 'jpeg', 'png', 'gif', 'bmp', 'svg', ]);
    }
}

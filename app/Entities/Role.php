<?php

namespace Pda\Entities;

use Illuminate\Database\Eloquent\Model;
use Pda\Entities\Contracts\RoleDefinition;

/**
 * Role model
 *
 * @package    Pda
 * @subpackage Entities
*/
class Role extends Model implements RoleDefinition
{
    //
}

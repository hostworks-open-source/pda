<?php

namespace Pda\Entities;

use Illuminate\Database\Eloquent\Model;
use Pda\Entities\Assessment;
use Pda\Entities\Contracts\AssessmentGroupDefinition;

/**
 * Assessment group model
 *
 * @package    Pda
 * @subpackage Entities
*/
class AssessmentGroup extends Model implements AssessmentGroupDefinition
{
    protected $table = 'assessment_groups';

    protected $fillable = [ 'name', ];

    public $timestamps = false;

    /**
     * An assessment group has many assessments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function assessments()
    {
        return $this->hasMany(Assessment::class);
    }


    /**
     * Get the corresponding submission field's name to store this group's ranking in.
     *
     * @return string
     */
    public function getSubmissionFieldAttribute()
    {
        switch ($this->attributes['id']) {
            case static::PARTNER:
                return 'partner_organisation_rank';
                break;
            case static::ROUND_ONE:
                return 'round_one_rank';
                break;
            case static::ROUND_TWO:
                return 'round_two_rank';
                break;
        }
    }
}

<?php

namespace Pda\Entities;

use Pda\Entities\User;
use Pda\Entities\Panel;
use Pda\Entities\Category;
use Pda\Entities\AssessmentGroup;
use Illuminate\Database\Eloquent\Model;

/**
 * Panel model
 *
 * @package    Pda
 * @subpackage Entities
*/
class Panel extends Model
{
    protected $fillable = [ 'name', 'max_applicants', ];

    /**
     * A panel belongs to many users.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function judges()
    {
        return $this->belongsToMany(User::class);
    }


    /**
     * A panel belongs to a category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }


    /**
     * A panel has many applications.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function applications()
    {
        return $this->belongsToMany(Submission::class);
    }


    /**
     * A panel belongs to an assessment group.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assessmentGroup()
    {
        return $this->belongsTo(AssessmentGroup::class);
    }


    /**
     * Return the number of applicants available in this panel.
     *
     * @return int
     */
    public function slotsAvailable()
    {
        return $this->max_applicants - ($this->applications->count());
    }


    /**
     * For the panel, get the database field that determines prior
     * eligibility to be ranked withinin this panel i.e. return
     * the database field that was used to mark the previous
     * round of ranking, which is necessary to progress.
     *
     * @return string|null
     */
    public function getEligibilityRankingAttribute()
    {
        switch ($this->assessment_group_id) {
            case AssessmentGroup::ROUND_ONE:
                return 'partner_organisation_rank';
                break;
            case AssessmentGroup::ROUND_TWO:
                return 'round_one_rank';
                break;
        }
    }
}

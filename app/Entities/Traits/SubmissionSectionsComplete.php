<?php

namespace Pda\Entities\Traits;

/**
 * Handle determining if a given submission section is complete
 *
 * @package    Pda
 * @subpackage Entities
*/
trait SubmissionSectionsComplete
{
    /**
     * Get all submission fields that relate to the given section.
     *
     * @throws \Exception If an invalid section has been specified.
     *
     * @return array
     */
    public function getSectionFields($section)
    {
        switch ($section) {
            case self::SECTION_DETAILS:
                return [
                    'category_id',
                    'partner_id',
                    'status_id',
                    'performer_name',
                    'address_1',
                    'city',
                    'state',
                    'postcode',
                    'country',
                    'phone_number',
                    'profile_image',
                ];
            break;
            case self::SECTION_ANSWERS:
                return [
                    'proof_of_identity',
                    'biography',
                    'achievements',
                    'current_engagements',
                    'ambitions_plan_of_action',
                    'apra_career_advancement',
                ];
            break;
            default:
                throw new \Exception('Invalid submission section specified.');
                break;
        }
    }


    /**
     * Determine whether or not the details section has been completed.
     *
     * @return boolean
     */
    public function getDetailsCompleteAttribute()
    {
        if (trim($this->user->name_first) === '' || trim($this->user->name_last) === '') {
            return false;
        }

        foreach ($this->getSectionFields(self::SECTION_DETAILS) as $field) {
            if (trim($this->attributes[$field]) === '') {
                return false;
            }
        }

        return true;
    }


    /**
     * Determine whether or not the work section has been completed.
     *
     * @return boolean
     */
    public function getWorkCompleteAttribute()
    {
        if (! $this->category_id) {
            return false;
        }

        $fields        = $this->categoryFileRequirements($this->category_id);
        $validRequired = $fields->count();
        $validPresent  = 0;

        foreach ($this->files as $file) {
            $field = $fields->shift();

            // Check if the given file requires title or description set, and verify whether
            // or not the corresponding field field is set. If not, skip to the next file.
            if (($field['title'] && trim($file->title) === '') || ($field['description'] && trim($file->description) === '')) {
                continue;
            }

            $validPresent++;
        };

        return $validPresent >= $validRequired;
    }


    /**
     * Determine whether or not the answers section has been completed.
     *
     * @return boolean
     */
    public function getAnswersCompleteAttribute()
    {
        foreach ($this->getSectionFields(self::SECTION_ANSWERS) as $field) {
            if (trim($this->attributes[$field]) === '') {
                return false;
            }
        }

        return true;
    }
}

<?php

namespace Pda\Entities\Traits;

use Pda\Entities\Role;

/**
 * Trait responsible for creating users of various types
 *
 * @package    Pda
 * @subpackage Entities
*/
trait CreatesUsers
{
    /**#@+
     * We expose this method as static so that we don't have to hardcode the
     * user type in other places in the application, which also means we 
     * have to worry less about users trying to set their own id when 
     * submitting the relevant forms. This also provides a verbose 
     * method name that clearly explains what is expected.
     *
     * @static
     *
     * @param  array $attributes Attributes to create user with
     *
     * @return \Illuminate\Database\Eloquent\Model
     */


    /**
     * Create an admin user.
     */
    public static function createAdmin(array $attributes = [ ])
    {
        return self::createUserOfType(Role::ADMIN, $attributes);
    }


    /**
     * Create a partner user.
     */
    public static function createPartner(array $attributes = [ ])
    {
        return self::createUserOfType(Role::PARTNER, $attributes);
    }


    /**
     * Create a staff user.
     */
    public static function createStaff(array $attributes = [ ])
    {
        return self::createUserOfType(Role::STAFF, $attributes);
    }


    /**
     * Create a judge user
     */
    public static function createJudge(array $attributes = [ ])
    {
        return self::createUserOfType(Role::JUDGE, $attributes);
    }


    /**
     * Create a user of type applicant.
     */
    public static function createApplicant(array $attributes = [ ])
    {
        return self::createUserOfType(Role::APPLICANT, $attributes);
    }
    /**#@-*/


    /**
     * Create a user of the given role.
     *
     * @param  int    $role_id    Identifier for the user role
     * @param  array  $attributes Attributes with which to create the user
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    private static function createUserOfType($role_id, array $attributes = [ ])
    {
        $user = self::create($attributes);
        $user->roles()->attach($role_id);

        return $user;
    }
    /**#@-*/
}

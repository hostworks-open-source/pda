<?php

namespace Pda\Entities;

use Iatstuti\Database\Support\NullableFields;
use Illuminate\Database\Eloquent\Model;
use Laracasts\Presenter\PresentableTrait;
use Pda\Entities\Submission;
use Pda\Entities\User;
use Pda\Presenters\VerificationPresenter;

/**
 * Submission verification model
 *
 * @package    Pda
 * @subpackage Entities
*/
class Verification extends Model
{
    use NullableFields, PresentableTrait;

    protected $presenter = VerificationPresenter::class;

    protected $fillable = [ 'accepted', 'reason', 'can_resubmit', ];

    protected $nullable = [ 'submission_id', 'user_id', 'reason', ];


    /**
     * A submission rejection belongs to a submission.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function submission()
    {
        return $this->belongsTo(Submission::class);
    }


    /**
     * A submission rejection belongs to a user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    /**
     * Strip tags when setting the reason attribute.
     *
     * @param $reason
     */
    public function setReasonAttribute($reason)
    {
        $this->attributes['reason'] = strip_tags($reason);
    }
}

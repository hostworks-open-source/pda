<?php

namespace Pda\Entities;

use Illuminate\Database\Eloquent\Model;
use Pda\Entities\Contracts\StatusDefinition;
use Pda\Entities\Submission;

/**
 * Status model
 *
 * @package    Pda
 * @subpackage Entities
*/
class Status extends Model implements StatusDefinition
{
    /**
     * Store valid status types.
     *
     * @var array
     */
    protected $valid_types = [ 'submission', ];


    /**
     * Determine if the given status type is valid.
     *
     * @param $type
     *
     * @return bool
     */
    public function validType($type)
    {
        return in_array($type, $this->valid_types);
    }


    /**
     * A status has many submissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function submissions()
    {
        return $this->hasMany(Submission::class);
    }
}

<?php

namespace Pda\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Pda\Entities\Category;
use Pda\Entities\Submission;

/**
 * Partner model
 *
 * @package    Pda
 * @subpackage Entities
*/
class Partner extends Model
{
    use SoftDeletes;

    protected $fillable = [ 'name', 'max_applicants', ];


    /**
     * A partner has many submissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function submissions()
    {
        return $this->hasMany(Submission::class);
    }


    /**
     * A partner belongs to many categories.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}

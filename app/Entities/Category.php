<?php

namespace Pda\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Pda\Entities\Contracts\CategoryDefinition;
use Pda\Entities\Panel;
use Pda\Entities\Partner;
use Pda\Entities\Submission;

/**
 * Category model.
 *
 * @package    Pda
 * @subpackage Entities
*/
class Category extends Model implements CategoryDefinition
{
    use SoftDeletes;

    protected $fillable = [ 'name', 'max_shortlist', ];


    /**
     * A category has many submissions.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function submissions()
    {
        return $this->hasMany(Submission::class);
    }


    /**
     * A category belongs to many partners.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function partners()
    {
        return $this->belongsToMany(Partner::class);
    }


    /**
     * A category has many panels.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function panels()
    {
        return $this->hasMany(Panel::class)->orderBy('name', 'asc');
    }


    /**
     * For the given category id, return only partners that have not
     * exceeded their maximum number of assignable applications.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query The query builder instance
     * @param  int                                   $category_id The category identifier
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAvailablePartners($query, $category_id)
    {
        return $query->find($category_id)
                     ->partners()
                     ->has('submissions', '<', \DB::raw('max_applicants'))
                     ->orderBy('name', 'asc');
    }
}

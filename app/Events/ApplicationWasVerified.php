<?php

namespace Pda\Events;

use Pda\Entities\Submission;
use Pda\Events\Event;
use Illuminate\Queue\SerializesModels;

/**
 * Application was verified event
 *
 * @package    Pda
 * @subpackage Events
*/
class ApplicationWasVerified extends Event
{
    use SerializesModels;

    /**
     * @var Submission
     */
    public $submission;

    /**
     * @var boolean
     */
    public $accepted;

    /**
     * @var string|null
     */
    public $reason;

    /**
     * @var bool
     */
    public $resubmit;


    /**
     * Create a new event instance.
     *
     * @param Submission  $submission The submission record
     * @param boolean     $accepted   Whether or not the application was accepted
     * @param string|null $reason     If the application was rejected, a reason may be provided
     * @param bool        $resubmit   If the application was rejected, it may be resubmitted
     */
    public function __construct(Submission $submission, $accepted = true, $reason = null, $resubmit = false)
    {
        $this->submission = $submission;
        $this->accepted   = $accepted;
        $this->reason     = trim($reason) == '' ? null : $reason;
        $this->resubmit   = $resubmit;
    }
}

<?php namespace Pda\Events;

use Pda\Events\Event;
use Pda\Entities\Submission;
use Pda\Entities\AssessmentGroup;
use Illuminate\Queue\SerializesModels;

/**
 * Event fired when an assessment was either created or deleted.
 *
*/
class AssessmentWasCreatedOrDeleted extends Event {

	use SerializesModels;

	/**
	 * Submission instance.
	 *
	 * @var Pda\Entities\Submission
	 */
	public $submission;

	/**
	 * Assessment group instance.
	 *
	 * @var Pda\Entities\AssessmentGroup
	 */
	public $assessmentGroup;

	/**
	 * Create a new event instance.
	 *
	 * @param  Submission      $submission
	 * @param  AssessmentGroup $assessmentGroup
	 *
	 * @return void
	 */
	public function __construct(Submission $submission, AssessmentGroup $assessmentGroup)
	{
		$this->submission      = $submission->fresh();
		$this->assessmentGroup = $assessmentGroup;
	}

}

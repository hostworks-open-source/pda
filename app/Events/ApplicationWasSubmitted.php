<?php

namespace Pda\Events;

use Pda\Entities\Submission;
use Pda\Events\Event;
use Illuminate\Queue\SerializesModels;

/**
 * An event to be fired when an application is submitted
 *
 * @package    Pda
 * @subpackage Events
*/
class ApplicationWasSubmitted extends Event
{
    use SerializesModels;

    /**
     * @var Submission
     */
    public $submission;


    /**
     * Create a new event instance.
     *
     * @param Submission $submission
     */
    public function __construct(Submission $submission)
    {
        $this->submission = $submission;
    }
}

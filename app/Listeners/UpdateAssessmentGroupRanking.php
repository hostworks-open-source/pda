<?php namespace Pda\Listeners;

use Pda\Events\AssessmentWasCreatedOrDeleted;

/**
 * Update the round assessment average when an
 * assessment is either created or deleted.
 *
*/
class UpdateAssessmentGroupRanking
{
    /**
     * Handle the fired event.
     *
     * @param  AssessmentWasCreatedOrDeleted $event
     *
     * @return void
     */
    public function handle(AssessmentWasCreatedOrDeleted $event)
    {
        $submission = $event->submission;
        $group      = $event->assessmentGroup;

        $submission->update([
            $group->submission_field => $submission->getRoundAssessmentAverage($group),
        ]);
    }
}

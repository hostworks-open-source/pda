<?php

namespace Pda\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Mail;
use Pda\Events\ApplicationWasSubmitted;

/**
 * Notify a user that their application was received
 *
 * @package    Pda
 * @subpackage Listeners
*/
class NotifyUserApplicationWasReceived implements ShouldBeQueued
{
    use InteractsWithQueue;


    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * Handle the event.
     *
     * @param  ApplicationWasSubmitted $event
     *
     * @return void
     */
    public function handle(ApplicationWasSubmitted $event)
    {
        $submission = $event->submission;
        $submission->load('user');

        Mail::send('emails.application.received', compact('submission'), function ($message) use ($submission) {
            $message->to($submission->user->email, $submission->user->name)
                ->subject('Your application for the APRA 2015 Professional Development Awards');
        });
    }
}

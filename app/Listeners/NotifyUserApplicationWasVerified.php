<?php

namespace Pda\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Mail;
use Pda\Events\ApplicationWasVerified;

/**
 * Notify a user their application was verified
 *
 * @package    Pda
 * @subpackage Listeners
*/
class NotifyUserApplicationWasVerified implements ShouldBeQueued
{
    use InteractsWithQueue;


    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    /**
     * Handle the event.
     *
     * @param  ApplicationWasVer $event
     *
     * @return void
     */
    public function handle(ApplicationWasVerified $event)
    {
        $submission = $event->submission;
        $accepted   = $event->accepted;
        $reason     = $event->reason;
        $resubmit   = $event->resubmit;
        $submission->load('user');

        Mail::send('emails.application.verified', compact('submission', 'accepted', 'reason', 'resubmit'), function ($message) use ($submission) {
            $message->to($submission->user->email, $submission->user->name)
                ->subject('Your application for the APRA 2015 Professional Development Awards');
        });
    }
}

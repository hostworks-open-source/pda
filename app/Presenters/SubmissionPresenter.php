<?php

namespace Pda\Presenters;

/**
 * Presenter for the submission model
 *
 * @package    Pda
 * @subpackage Presenters
*/
class SubmissionPresenter extends Presenter
{
    /**
     * Present the biography field.
     *
     * @return string
     */
    public function biography()
    {
        return nl2br($this->entity->biography);
    }


    /**
     * Present the achievements field.
     *
     * @return string
     */
    public function achievements()
    {
        return nl2br($this->entity->achievements);
    }


    /**
     * Present the current engagements field.
     *
     * @return string
     */
    public function currentEngagements()
    {
        return nl2br($this->entity->current_engagements);
    }


    /**
     * Present the ambitions and plan of action field.
     *
     * @return string
     */
    public function ambitionsPlanOfAction()
    {
        return nl2br($this->entity->ambitions_plan_of_action);
    }


    /**
     * Present the APRA career advancement field.
     *
     * @return string
     */
    public function apraCareerAdvancement()
    {
        return nl2br($this->entity->apra_career_advancement);
    }
}

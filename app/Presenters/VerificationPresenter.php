<?php

namespace Pda\Presenters;

/**
 * Verification presenter
 *
 * @package    Pda
 * @subpackage Presenters
*/
class VerificationPresenter extends Presenter
{
    /**
     * Present the reason field.
     *
     * @return string
     */
    public function reason()
    {
        return nl2br($this->entity->reason);
    }
}

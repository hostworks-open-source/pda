<?php

namespace Pda\Presenters;

use Laracasts\Presenter\Presenter as LaracastsPresenter;

/**
 * Extend Laracasts' presenter class in order to correct isset behaviour
 *
 * @package    Pda
 * @subpackage Presenters
*/
abstract class Presenter extends LaracastsPresenter
{
    /**
     * Provide compatibility for ternary syntax.
     *
     * @param $property
     *
     * @return bool
     */
    public function __isset($property)
    {
        return method_exists($this, $property);
    }
}

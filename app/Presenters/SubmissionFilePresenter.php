<?php

namespace Pda\Presenters;

/**
 * Submission file presenter
 *
 * @package    Pda
 * @subpackage Presenters
*/
class SubmissionFilePresenter extends Presenter
{
    /**
     * Present the description field.
     *
     * @return string
     */
    public function description()
    {
        return nl2br($this->entity->description);
    }
}

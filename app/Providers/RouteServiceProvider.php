<?php namespace Pda\Providers;

use Hashids;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use Pda\Entities\Panel;
use Pda\Entities\Submission;
use Pda\Exceptions\SubmissionNotFoundException;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'Pda\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->bind('submission', function ($hash) {
            $id = Hashids::decode($hash);
            
            if (! $id || ! ($submission = Submission::find($id)->first())) {
                throw new SubmissionNotFoundException("Submission {$hash} not found");
            }

            return $submission;
        });

        $router->model('panel', Panel::class);

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}

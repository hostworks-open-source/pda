<?php namespace Pda\Providers;

use Illuminate\Contracts\Validation\ValidationException;
use Illuminate\Support\ServiceProvider;
use Pda\Entities\Category;
use Validator;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('max_words', function ($attribute, $value, $parameters) {
            return count(preg_split('/\s+/', $value)) <= $parameters[0];
        });

        Validator::replacer('max_words', function ($message, $attribute, $rule, $parameters) {
            return str_replace(':word_count', $parameters[0], $message);
        });

        Validator::extend('valid_partner', function ($attribute, $value, $parameters) {
            return ! is_null(Category::availablePartners($parameters[0])->where('partners.id', $parameters[1])->first());
        });
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('Illuminate\Contracts\Auth\Registrar', 'Pda\Services\Registrar');

        // Ensure IDE helper and debugbar are available only in local environment
        if ($this->app->environment() == 'local') {
            $this->app->register('Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider');
            $this->app->register('Barryvdh\Debugbar\ServiceProvider');
            $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
        }
    }
}

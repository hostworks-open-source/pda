<?php

/**
 * Generate a link to provide filtering of submission records.
 *
 * @param  string $column Column to filter by
 * @param  string $label  Label for link
 * @return string
 */
function sort_submissions_by($column, $label)
{
    $filters = array_merge(Request::only([
        'name',
        'email',
        'status_id',
        'category_id',
        'partner_organisation_rank',
        'submitted_at',
        'sort',
        'order'
    ]), [
        'sort'  => $column,
        'order' => Request::get('order') == 'asc' ? 'desc' : 'asc',
    ]);

    $current_order = get_sort_order();
    $chevron       = get_chevron($column, $current_order);

    return sprintf('<a href="?%s">%s%s</a>', http_build_query($filters, null, '&'), $label, $chevron);
}

/**
 * Return the direction chevron for this request.
 *
 * @param  string $column        Column to check current request against
 * @param  string $current_order Current request order
 * @return string|null
 */
function get_chevron($column, $current_order)
{
    if (Request::get('sort') == $column) {
        return sprintf(' <i class="fa fa-caret-%s" title="Chevron 7; locked!"></i>', $current_order == 'asc' ? 'up' : 'down');
    }

    return null;
}

/**
 * Return the current sort order.
 *
 * @return string
 */
function get_sort_order()
{
    return Request::get('order') ?: 'asc';
}

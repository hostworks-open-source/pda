<?php

namespace Pda;

use Illuminate\Support\Collection;
use Pda\Entities\Category;

/**
 * Define requirements for each submission file in a given category
 */
trait IdentifiesSubmissionFileRequirements
{
    /**
     * Get category file requirements for the given category.
     *
     * @param  int $categoryId Category to determine requirements for
     * @return array
     */
    public function categoryFileRequirements($categoryId)
    {
        switch ($categoryId) {
            case Category::POPULAR_CONTEMPORARY:
            case Category::COUNTRY:
            case Category::ATSI:
                return new Collection([
                    'submission_file_1' => [ 'title' => true, 'description' => true, ],
                    'submission_file_2' => [ 'title' => true, 'description' => true, ],
                    'submission_file_3' => [ 'title' => true, 'description' => true, ],
                 ]);
                break;
            case Category::CLASSICAL:
            case Category::JAZZ:
            case Category::FILM_AND_TELEVISION:
                return new Collection([
                    'submission_file_1' => [ 'title' => true, 'description' => false, ],
                    'submission_file_2' => [ 'title' => true, 'description' => false, ],
                    'submission_file_3' => [ 'title' => true, 'description' => false, ],
                 ]);
                break;
            default:
                // no-op
                break;
        }
    }
}

<?php

namespace Pda;

use Pda\Entities\Submission;
use Storage;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * Handles uploading of submission files

 */
trait UploadsApplicationFiles
{
    /**
     * Upload the submission's profile image
     *
     * @param  string     $file
     * @param  string     $identifier
     * @param  Submission $submission
     *
     * @return bool|string False if the file was invalid or upload failed, else the saved filename
     */
    public function uploadFile($file, $identifier, Submission $submission)
    {
        if (! $file || ! $file->isValid()) {
            return false;
        }

        $filename = sprintf('%s_%s_%s.%s', $submission->hash, $identifier, md5($file->getClientOriginalName()), $file->getClientOriginalExtension());

        try {
            $file->move(config('laravel-glide.source.path'), $filename);
            
            // Ensure that if the previous image for this identifier exists that we delete it
            $fs = Storage::disk('images');

            if ($submission->{$identifier} && $fs->exists($submission->{$identifier})) {
                $fs->delete($submission->{$identifier});
            }

            return $filename;
        } catch (FileException $e) {
            // no-op
        }

        return false;
    }
}

<?php

namespace Pda;

use Storage;

/**
 * Calculates usage of files in a directory
 */
trait CalculatesApplicationUsage
{
    /**
     * Determine how much space is being used by a submission directory.
     *
     * @param $directory
     *
     * @return int
     */
    private function getSpaceUsed($directory)
    {
        $total = 0;
        $this->fs = Storage::disk('submissions');

        foreach ($this->fs->files($directory) as $file) {
            $total += $this->fs->size($file);
        }

        return $total;
    }
}

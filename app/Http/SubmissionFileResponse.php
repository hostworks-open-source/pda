<?php

namespace Pda\Http;

/**
 * Submission file response
 *
 * @package    Pda
 * @subpackage Http
*/
trait SubmissionFileResponse
{
    use JsonResponse;

    /**
     * There were too many files.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respondTooManyFiles()
    {
        return $this->setMessage(sprintf('You may only upload %d files with your submission', env('SUBMISSION_TOTAL_FILES')))
            ->respondNotAcceptable();
    }


    /**
     * The filesize limit was exceeded.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respondFilesizeLimitExceeded()
    {
        return $this->setMessage(sprintf('The total size of all files must be below %s', env('SUBMISSION_TOTAL_SIZE_HUMAN')))
            ->respondTooLarge();
    }


    /**
     * The file already exists.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respondFileExists()
    {
        return $this->setMessage('The file you are trying to upload already exists')->respondUnprocessable();
    }


    /**
     * The file was uploaded.
     *
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respondFileUploaded(array $data = [ ])
    {
        return $this->setMessage('The file was successfully uploaded')->setData($data)->respondOk();
    }


    /**
     * The filename was removed.
     *
     * @param $filename
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respondFileDeleted($filename)
    {
        return $this->setMessage('Successfully removed file ' . $filename)->respondOk();
    }


    /**
     * The file does not exist.
     *
     * @param $filename
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respondFileNotExist($filename)
    {
        return $this->setMessage(sprintf('The requested file [%s] does not exist', $filename))->respondNotFound();
    }


    /**
     * User not authorised to remove file.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respondFileNotAuthorised()
    {
        return $this->setMessage('You are not authorised to remove this file')->respondNotAuthorised();
    }
}

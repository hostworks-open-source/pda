<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

get('/home', function () {
    return redirect()->route('home');
});

get('/', [ 'as' => 'home', function () {
    $browserVersion = sprintf('%s %s', Agent::browser(), Agent::version(Agent::browser()));
    return view('home', compact('browserVersion'));
} ]);

Route::group([ 'prefix' => 'submission', ], function () {
    get('/mine', [ 'as' => 'my.submission', 'uses' => 'SubmissionController@my', ]);
    get('/create', [ 'as' => 'submission.create', 'uses' => 'SubmissionController@create', ]);
    post('/', [ 'as' => 'submission.store', 'uses' => 'SubmissionController@store', ]);

    Route::group([ 'prefix' => '{submission}', ], function () {
        get('/edit', [ 'as' => 'submission.edit', 'uses' => 'SubmissionController@edit', ]);
        put('/update', [ 'as' => 'submission.update', 'uses' => 'SubmissionController@update', ]);
        get('/review', [ 'as' => 'submission.review', 'uses' => 'SubmissionController@review', ]);
        post('/submit', [ 'as' => 'submission.submit', 'uses' => 'SubmissionController@submit', ]);
        post('/verify', [ 'as' => 'submission.verify', 'uses' => 'SubmissionController@verify', ]);

        post('/upload', [ 'as' => 'submission.fileupload', 'uses' => 'SubmissionFileController@upload', ]);
        delete('/destroy', [ 'as' => 'submission.deletefile', 'uses' => 'SubmissionFileController@destroy', ]);

        get('/media/{media}', [ 'as' => 'submission.media', 'uses' => 'SubmissionMediaController@media', ]);
        post('/assess', [ 'as' => 'submission.assess', 'middleware' => 'submission.assess', 'uses' => 'AssessmentController@assess', ]);
    });
});

Route::group([ 'prefix' => 'admin', 'namespace' => 'Admin' ], function () {
    Route::group([ 'middleware' => 'is.admin', ], function () {
        get('/', [ 'as' => 'admin.index', 'uses' => 'AdminController@index', ]);
        get('/submissions', [ 'as' => 'admin.applications.index', 'uses' => 'ApplicationController@index', ]);

        Route::group([ 'prefix' => 'categories', ], function () {
            get('/', [ 'as' => 'admin.categories.index', 'uses' => 'CategoryController@index', ]);
            post('/update', [ 'as' => 'admin.categories.update', 'uses' => 'CategoryController@update', ]);
        });

        Route::group([ 'prefix' => 'judging', ], function () {
            Route::group([ 'prefix' => 'panels', ], function () {
                get('/', [ 'as' => 'admin.judging.panels.index', 'uses' => 'JudgingPanelsController@index', ]);
                Route::group([ 'prefix' => '{panel}', ], function () {
                    post('/', [ 'as' => 'admin.judging.panels.update', 'uses' => 'JudgingPanelsController@update', ]);
                    get('/edit', [ 'as' => 'admin.judging.panels.edit', 'uses' => 'JudgingPanelsController@edit', ]);
                    get('/detach/{submission}', [ 'as' => 'admin.judging.panels.detachSubmission', 'uses' => 'JudgingPanelsController@detachSubmission', ]);
                    get('/attach', [ 'as' => 'admin.judging.panels.attachSubmissionIndex', 'uses' => 'JudgingPanelsController@attachSubmissionIndex', ]);
                    get('/attach/{submission}', [ 'as' => 'admin.judging.panels.attachSubmission', 'uses' => 'JudgingPanelsController@attachSubmission', ]);
                });
            });
        });
    });

    Route::group([ 'middleware' => 'is.partner', ], function () {
        get('/applications/partners/accepted', [ 'as' => 'admin.applications.accepted.index', 'uses' => 'PartnerController@index', ]);
        get('/applications/partners/completed', [ 'as' => 'admin.applications.completed.index', 'uses' => 'PartnerController@completed', ]);
        get('/applications/partners/{submission}/rank', [ 'as' => 'submission.rank.partner', 'uses' => 'PartnerController@rank', ]);
    });

    Route::group([ 'middleware' => 'is.judge', ], function () {
        get('/applications/judges/shortlisted', [ 'as' => 'admin.applications.shortlisted.index', 'uses' => 'JudgeController@index', ]);
        get('/applications/judges/shortlisted/{panel}', [ 'as' => 'admin.applications.shortlisted.show', 'uses' => 'JudgeController@show', ]);
        get('/applications/judges/shortlisted/{panel}/completed', [ 'as' => 'admin.applications.shortlisted.show.completed', 'uses' => 'JudgeController@completed', ]);
        get('/applications/judges/{submission}/rank/panel/{panel}', [ 'as' => 'submission.rank.judge', 'uses' => 'JudgeController@rank', ]);
        get('/applications/judges/{submission}/rank/remove/panel/{panel}', [ 'as' => 'submission.rank.judge.remove', 'uses' => 'JudgeController@remove', ]);
    });
});

Route::group([ 'prefix' => 'api', ], function () {
    get('/category/partners', [ 'as' => 'api.category.partners', 'uses' => 'ApiController@availableCategoryPartners', ]);
    get('/category/partners/all', [ 'as' => 'api.category.partnersAll', 'uses' => 'ApiController@allCategoryPartners', ]);
    get('/submission/file_requirements', [ 'as' => 'api.submission.file_requirements', 'uses' => 'ApiController@submissionFileRequirements', ]);
});

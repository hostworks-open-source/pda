<?php

namespace Pda\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

/**
 * Middleware to determine if user can review an application
 *
 * @package    Pda
 * @subpackage Http\Middleware
*/
class CanReviewApplication
{
    /**
     * @var Guard
     */
    private $auth;


    /**
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest() || ($request->submission->user_id !== $this->auth->id()) && ! $this->auth->user()->hasRole('Staff')) {
            flash('You do not have permission to review the requested application');

            return redirect()->route('home');
        }

        return $next($request);
    }
}

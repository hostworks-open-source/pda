<?php

namespace Pda\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

/**
 * Middleware to determine if logged in user (if present) is a judge
 *
 * @package    Pda
 * @subpackage Http\Middleware
*/
class IsJudge
{
    /**
     * @var Guard
     */
    private $auth;


    /**
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest() || ! $this->auth->user()->hasRole('Staff', 'Judge')) {
            flash('Only judges can access this page.');

            return redirect()->route('home');
        }

        return $next($request);
    }
}

<?php

namespace Pda\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

/**
 * Determine if the visitor is an admin user
 *
 * @package    Pda
 * @subpackage Http\Middleware
*/
class IsAdmin
{
    /**
     * @var Guard
     */
    private $auth;


    /**
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest() || ! $this->auth->user()->hasRole('Staff', 'Admin')) {
            flash('You must be logged in to access this page.');

            return redirect()->guest('auth/login');
        }

        return $next($request);
    }
}

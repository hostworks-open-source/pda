<?php

namespace Pda\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

/**
 * Determine if the logged in user can submit this application
 *
 * @package    Pda
 * @subpackage Http\Middleware
*/
class CanSubmitApplication
{
    /**
     * @var Guard
     */
    private $auth;


    /**
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest() || ($this->auth->id() !== $request->submission->user_id)) {
            flash('You do not have permission to submit the requested application.');

            return redirect()->route('home');
        }

        return $next($request);
    }
}

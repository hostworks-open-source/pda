<?php

namespace Pda\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

/**
 * Middleware to determine whether the logged in user can assess an application
 *
 * @package    Pda
 * @subpackage Http\Middleware
*/
class CanAssessApplication
{
    /**
     * @var  Guard
     */
    private $auth;


    /**
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest() || ! $this->auth->user()->hasRole('Staff', 'Admin', 'Partner', 'Judge')) {
            flash('You are not authorised to assess this application.');

            return redirect()->route('home');
        }

        // Perform these checks for non-superusers, as we will achieve false
        // positives otherwise, given that superusers will have all roles.
        if (! $this->auth->user()->isSuperUser()) {
            if ($this->rankingClosed('Judge', env('JUDGING_CLOSE_DATE_JUDGES'))) {
                flash('Submission of judge rankings closed on ' . date('l jS F, Y \a\t g:ia', strtotime(env('JUDGING_CLOSE_DATE_JUDGES'))));

                return redirect()->route('home');
            }

            if ($this->rankingClosed('Partner', env('JUDGING_CLOSE_DATE_PARTNERS')) && ! $this->auth->user()->hasRole('Judge')) {
                flash('Submission of partner organisation rankings closed on ' . date('l jS F, Y \a\t g:ia', strtotime(env('JUDGING_CLOSE_DATE_PARTNERS'))));

                return redirect()->route('home');
            }
        }

        return $next($request);
    }


    /**
     * Determine whether ranking has closed for a given user role.
     *
     * @param  string $role     Role to check against
     * @param  string $datetime Closing datetime (YYYY-MM-DD HH:mm:ss) for this role
     *
     * @return boolean
     */
    private function rankingClosed($role, $datetime)
    {
        return $this->auth->user()->hasRole($role) && ( time() > strtotime($datetime) );
    }
}

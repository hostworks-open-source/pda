<?php

namespace Pda\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Pda\Entities\Submission;

/**
 * Middleware to determine if the logged in user can edit the given profile
 *
 * @package    Pda
 * @subpackage Http\Middleware
*/
class CanEditProfile
{
    /**
     * @var Guard
     */
    private $auth;


    /**
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            flash('You must be logged in to edit the requested profile');

            return redirect()->guest('auth/login');
        }

        if ($request->submission->user_id !== $this->auth->id() && ! $this->auth->user()->hasRole('Staff')) {
            flash('You do not have permission to view the requested profile');

            return redirect()->route('home');
        }

        return $next($request);
    }
}

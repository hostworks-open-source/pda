<?php

namespace Pda\Http\Middleware;

use Closure;

/**
 * Middleware to verify whether or not applications are open
 *
 * @package    Pda
 * @subpackage Http\Middleware
*/
class AreApplicationsOpen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (( time() >= strtotime(env('APPLICATION_CLOSE_TIMESTAMP'))) && ! auth()->user()->hasRole('Staff')) {
            flash(sprintf(
                'Submissions for the 2015 APRA Professional Development Awards Closed on %s',
                date('l jS F, Y \a\t g:ia', strtotime(env('APPLICATION_CLOSE_TIMESTAMP')))
            ));

            return redirect()->route('home');
        }
        return $next($request);
    }
}

<?php namespace Pda\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        'Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode',
        'Illuminate\Cookie\Middleware\EncryptCookies',
        'Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse',
        'Illuminate\Session\Middleware\StartSession',
        'Illuminate\View\Middleware\ShareErrorsFromSession',
        'Pda\Http\Middleware\VerifyCsrfToken',
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth'              => 'Pda\Http\Middleware\Authenticate',
        'auth.basic'        => 'Illuminate\Auth\Middleware\AuthenticateWithBasicAuth',
        'guest'             => 'Pda\Http\Middleware\RedirectIfAuthenticated',
        'profile.edit'      => 'Pda\Http\Middleware\CanEditProfile',
        'submission.submit' => 'Pda\Http\Middleware\CanSubmitApplication',
        'submission.review' => 'Pda\Http\Middleware\CanReviewApplication',
        'submission.open'   => 'Pda\Http\Middleware\AreApplicationsOpen',
        'submission.assess' => 'Pda\Http\Middleware\CanAssessApplication',
        'is.admin'          => 'Pda\Http\Middleware\IsAdmin',
        'is.partner'        => 'Pda\Http\Middleware\IsPartner',
        'is.judge'          => 'Pda\Http\Middleware\IsJudge',
    ];
}

<?php

namespace Pda\Http\Requests;

use Pda\Http\Requests\Request;

/**
 * Create submission request
 *
 * @package    Pda
 * @subpackage Http\Requests
*/
class CreateSubmissionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_first' => 'required|min:2',
            'name_last'  => 'required|min:2',
            'email'      => 'required|email|unique:users',
            'password'   => 'required|min:8',
        ];
    }
}

<?php

namespace Pda\Http\Requests;

use Illuminate\Validation\Validator;
use Pda\Http\Requests\Request;

/**
 * Form request for verifying an application
 *
 * @package    Pda
 * @subpackage Http\Requests
*/
class VerifyApplicationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'accepted' => 'required|boolean',
        ];
    }


    /**
     * Override the failedValidation method so that we can flash an error message.
     *
     * @param  Validator $validator
     *
     * @return mixed
     */
    protected function failedValidation(Validator $validator)
    {
        flash()->error('You have not specified whether or not the application is accepted.');

        parent::failedValidation($validator);
    }
}

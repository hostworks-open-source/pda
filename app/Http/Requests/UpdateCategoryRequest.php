<?php

namespace Pda\Http\Requests;

use Illuminate\Validation\Validator;
use Pda\Http\Requests\Request;

/**
 * Update submission form request
 *
 * @package    Pda
 * @subpackage Http\Requests
*/
class UpdateCategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'     => 'required',
        ];

        if ($this->request->has('partners')) {
            foreach ($this->request->get('partners') as $key => $value) {
                $rules['partners.' . $key] = 'exists:partners,id';
            }
        }

        return $rules;
    }


    /**
     * Override the failedValidation method so that we flash a message.
     *
     * @param  Validator $validator Validator instance
     * @return mixed
     */
    protected function failedValidation(Validator $validator)
    {
        flash()->error('An error occurred when saving the category. Ensure the name is provided.');

        parent::failedValidation($validator);
    }
}

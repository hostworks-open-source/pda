<?php

namespace Pda\Http\Requests;

use Illuminate\Validation\Validator;
use Pda\Http\Requests\Request;

/**
 * Update submission request.
 *
 * @package    Pda
 * @subpackage Http\Requests
*/
class UpdateSubmissionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            // My details
            'category_id'              => 'sometimes|exists:categories,id',
            'name_first'               => 'sometimes|min:2',
            'name_last'                => 'sometimes|min:2',
            'performer_name'           => 'sometimes|min:2',
            'address_1'                => 'sometimes|min:2|max:255',
            'address_2'                => 'sometimes|min:2|max:255',
            'city'                     => 'sometimes|min:2|max:255',
            'state'                    => 'sometimes|min:2|max:255',
            'postcode'                 => 'sometimes|min:2|max:6',
            'country'                  => 'sometimes|min:2|max:255',
            'profile_image'            => 'sometimes|image',
            'phone_number'             => 'sometimes|min:6|max:10',
            // Answers
            'proof_of_identity'        => 'sometimes|image',
            'biography'                => 'sometimes|max_words:300',
            'achievements'             => 'sometimes|max_words:300',
            'current_engagements'      => 'sometimes|max_words:300',
            'ambitions_plan_of_action' => 'sometimes|max_words:300',
            'apra_career_advancement'  => 'sometimes|max_words:300',
        ];

        if ($this->partner_id) {
            $rules['partner_id'] = 'sometimes|exists:partners,id|valid_partner:' . $this->category_id . ',' . $this->partner_id;
        }

        return $rules;
    }


    /**
     * Override some of our attribute names to be more user friendly.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            // My details
            'category_id'             => 'category',
            'partner_id'              => 'partner organisation',
            'performer_name'          => 'performing name',
            'address_1'               => 'address line 1',
            'address_2'               => 'address line 2',
            'profile_image'           => 'profile picture',
            // Answers
            'apra_career_advancement' => 'APRA as career advancement',
        ];
    }


    /**
     * Override the failedValidation method so that we can flash an error message,
     * otherwise if the error exists on the work or answers tabs, which aren't
     * active on a page load, the sees no feedback as to what happened.
     *
     * @param  Validator $validator
     *
     * @return mixed
     */
    protected function failedValidation(Validator $validator)
    {
        if ($this->request->has('active_tab')) {
            session()->flash('active_tab', $this->request->get('active_tab'));
        }

        flash()->error('There was an error in your submission. Please check your input and try again.');

        parent::failedValidation($validator);
    }
}

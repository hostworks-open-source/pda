<?php

namespace Pda\Http\Requests;

use Illuminate\Validation\Validator;
use Pda\Http\Requests\Request;

/**
 * Update panel form request
 *
 * @package    Pda
 * @subpackage Http\Requests
*/
class UpdatePanelRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'         => 'required|exists:categories,id',
            'assessment_group_id' => 'required|exists:assessment_groups,id',
            'name'                => 'required|regex:/[a-z0-9 ]+/i',
            'max_applicants'      => 'required|integer|min:0',
        ];
    }


    /**
     * Overwrite method to ensure we flash an error message.
     *
     * @param  Validator $validator Validation instance
     *
     * @return mixed
     */
    protected function failedValidation(Validator $validator)
    {
        flash()->error('Please check your input and try again.');

        parent::failedValidation($validator);
    }
}

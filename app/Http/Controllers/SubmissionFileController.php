<?php

namespace Pda\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Pda\CalculatesApplicationUsage;
use Pda\Entities\Submission;
use Pda\Http\Controllers\Controller;
use Pda\Http\Requests;
use Pda\Http\SubmissionFileResponse;
use Storage;

/**
 * Submission file controller
 *
 * @package    Pda
 * @subpackage Http\Controllers
*/
class SubmissionFileController extends Controller
{
    use SubmissionFileResponse, CalculatesApplicationUsage;

    /**
     * Store a filesystem instance.
     *
     * @var \Illuminate\Contracts\Filesystem\Filesystem
     */
    protected $fs;


    /**
     * Class constructor
     */
    public function __construct()
    {
        $this->fs = Storage::disk('submissions');
    }


    /**
     * @param Request    $request
     * @param Submission $submission
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function upload(Request $request, Submission $submission)
    {
        $files = (array) $this->fs->files($submission->hash);
        $file  = $request->file('file');

        if (count($files) >= env('SUBMISSION_TOTAL_FILES')) {
            return $this->respondTooManyFiles();
        }

        // Check the space being used, make sure the new file doesn't cause us to go over allowance
        if (($this->getSpaceUsed($submission->hash) + $file->getClientSize()) > env('SUBMISSION_TOTAL_SIZE_BYTES')) {
            return $this->respondFilesizeLimitExceeded();
        }

        if ($this->fs->exists($submission->hash . '/' . $file->getClientOriginalName())) {
            return $this->respondFileExists();
        }

        $data = [
            'name' => $file->getClientOriginalName(),
            'size' => $file->getSize(),
            'type' => $file->getMimeType(),
        ];

        if (! $file->move(storage_path('applications/' . $submission->hash), $data['name'])) {
            return $this->respondBadRequest();
        }

        $submission->files()->create([
            'filename'    => $data['name'],
            'title'       => $request->get('title'),
            'description' => $request->get('description'),
            'filesize'    => $data['size'],
            'mime_type'   => $data['type'],
            'file_group'  => $request->get('file_group'),
        ]);

        return $this->respondFileUploaded($data);
    }


    /**
     * Destroy an existing submission file.
     *
     * @param Request    $request
     * @param Submission $submission
     *
     * @return mixed
     */
    public function destroy(Request $request, Submission $submission)
    {
        if (($submission->user->id !== auth()->user()->id) && ! auth()->user()->hasRole('Staff')) {
            return $this->respondFileNotAuthorised();
        }

        $submission_file = $submission->files()->where('filename', $request->get('file'))->first();

        if (! $submission_file) {
            return $this->respondFileNotExist($request->get('file'));
        }
        
        $this->fs = Storage::disk('submissions');

        $this->fs = Storage::disk('submissions');

        // If the file exists on disk, remove it
        if ($this->fs->exists($submission->hash . '/' . $request->get('file'))) {
            $this->fs->delete($submission->hash . '/' . $request->get('file'));
        }

        $submission_file->delete();

        $this->respondFileDeleted($request->get('file'));
    }
}

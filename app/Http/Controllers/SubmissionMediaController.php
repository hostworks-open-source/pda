<?php

namespace Pda\Http\Controllers;

use Illuminate\Http\Response;
use Pda\Entities\Submission;
use Pda\Entities\SubmissionFile;
use Pda\Http\Controllers\Controller;
use Pda\Http\Requests;
use Request;

/**
 * Submission media controller. Fetches and serves media from the filesystem.
 *
 * @package    Pda
 * @subpackage Http\Controllers
*/
class SubmissionMediaController extends Controller
{
    /**
     * Class constructor
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('submission.review', [ 'only' => [ 'stream', ], ]);
    }


    /**
     * Stream the requested file for the given submission.
     *
     * @param  Submission $submission Submission to stream the given media from
     * @param  string     $media      The file to be streamed
     * @return \Symfony\Component\HttpFoundation\Response|\Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function media(Submission $submission, $media)
    {
        $file = $submission->files()->where('filename', urldecode($media))->first();

        // Audio and video files need to be streamed to the browser, to facilitate
        // file chunking and ranges, in order to allow users to scrub playback.
        if (starts_with($file->mime_type, ['audio/', 'video/', ])) {
            return $this->streamResponse($file);
        }

        return response()->download($file->path, $file->filename, [
            'Content-Type'        => $file->mime_type,
            'Content-Length'      => $file->filesize,
            'Content-Disposition' => sprintf('inline; filename="%s"', $file->filename),
        ]);
    }


    /**
     * Stream a response file back to the browser.
     *
     * @param  SubmissionFile $file File to stream back
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    private function streamResponse(SubmissionFile $file)
    {
        $headers      = [ 'Content-Type' => $file->mime_type, ];
        $stream       = fopen($file->path, 'r');
        $size         = $file->filesize;
        $responseCode = Response::HTTP_OK;
        $range        = Request::header('Range');

        if (! is_null($range)) {
            $posCurrent = strpos($range, '=');
            $posTo      = strpos($range, '-');
            $unit       = substr($range, 0, $posCurrent);
            $start      = intval(substr($range, $posCurrent+1, $posTo));
            $success    = fseek($stream, $start);

            if (! $success) {
                $size         = $file->filesize - $start;
                $responseCode = Response::HTTP_PARTIAL_CONTENT;
                $headers['Accept-Ranges'] = $unit;
                $headers['Content-Range'] = sprintf('%s %s-%s/%s', $unit, $start, $file->filesize-1, $file->filesize);
            }
        }

        $headers['Content-Length'] = $size;

        return response()->stream(function () use ($stream) {
            fpassthru($stream);
        }, $responseCode, $headers);
    }
}

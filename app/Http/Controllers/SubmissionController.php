<?php

namespace Pda\Http\Controllers;

use Pda\Entities\Category;
use Pda\Entities\Partner;
use Pda\Entities\Status;
use Pda\Entities\Submission;
use Pda\Entities\SubmissionFile;
use Pda\Entities\User;
use Pda\Entities\Verification;
use Pda\Events\ApplicationWasSubmitted;
use Pda\Events\ApplicationWasVerified;
use Pda\Http\Requests;
use Pda\Http\Controllers\Controller;
use Pda\Http\Requests\CreateSubmissionRequest;
use Pda\Http\Requests\UpdateSubmissionRequest;
use Pda\Http\Requests\VerifyApplicationRequest;
use Pda\IdentifiesSubmissionFileRequirements;
use Pda\UploadsApplicationFiles;

/**
 * Submission controller
 *
 * @package    Pda
 * @subpackage Http\Controllers
*/
class SubmissionController extends Controller
{
    use UploadsApplicationFiles, IdentifiesSubmissionFileRequirements;

    /**
     * Class constructor.
     *
     * Register middleware here, rather than in the routes file.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('profile.edit', [ 'only' => 'edit', ]);
        $this->middleware('submission.submit', [ 'only' => 'submit', ]);
        $this->middleware('submission.review', [ 'only' => [ 'review', 'verify', ], ]);
        $this->middleware('submission.open', [ 'only' => [ 'create', 'edit', 'update', ], ]);
    }


    /**
     * Shortcut to redirect the user to their submission.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function my()
    {
        if (auth()->guest()) {
            flash('You must be logged in to view this page');

            return redirect()->guest('auth/login');
        }

        $submission = auth()->user()->submission;

        if (! $submission) {
            flash('You have not yet created an application');

            return redirect()->route('submission.create');
        }

        return redirect()->route('submission.edit', $submission->hash);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        // If we have a logged in user, and that user already has a submission, redirect them to it
        if (auth()->user() && auth()->user()->submission) {
            return redirect()->route('submission.edit', auth()->user()->submission->hash);
        }

        return view('submission.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param CreateSubmissionRequest $request
     *
     * @return Response
     */
    public function store(CreateSubmissionRequest $request)
    {
        // Create a new user for this submission.
        $user = User::createApplicant($request->only([ 'name_first', 'name_last', 'email', 'password', ]));

        // After creating the user, log them in immediately.
        auth()->loginUsingId($user->id);

        // Create a submission so we get an identifier
        $user->submission()->save(new Submission);

        return redirect()->route('submission.edit', $user->submission->hash);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Submission $submission
     *
     * @return Response
     */
    public function edit(Submission $submission)
    {
        if (! $submission->isPending && ! auth()->user()->hasRole('Staff')) {
            flash('This application has already been submitted and cannot be edited.');

            return redirect()->route('submission.review', $submission->hash);
        }

        $partners_disabled = true;
        $active_tab        = session()->get('active_tab') ?: 'details';

        $submission->load('user', 'files');
        $categories = [ '' => 'Select category', ] + Category::all()->lists('name', 'id');
        $partners   = [ '' => 'Select category first', ];

        if ($submission->category_id) {
            $partners_disabled = false;
            $fields_required   = $this->categoryFileRequirements($submission->category_id);
            $partners          = [ 0 => 'Select partner organisation', ] + Category::availablePartners($submission->category_id)->lists('name', 'id');
        }

        $submission_files = $submission->files->groupBy('file_group');

        return view('submission.edit', compact('submission', 'categories', 'partners', 'active_tab', 'partners_disabled', 'fields_required', 'submission_files'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UpdateSubmissionRequest $request
     * @param Submission              $submission
     *
     * @return Response
     */
    public function update(UpdateSubmissionRequest $request, Submission $submission)
    {
        $submission->user->name_first = $request->get('name_first');
        $submission->user->name_last  = $request->get('name_last');
        $submission->user->save();

        if ($profile_image = $this->uploadFile($request->file('profile_image'), 'profile_image', $submission)) {
            $submission->fill([ 'profile_image' => $profile_image, ]);
        }

        if ($proof_of_identity = $this->uploadFile($request->file('proof_of_identity'), 'proof_of_identity', $submission)) {
            $submission->fill([ 'proof_of_identity' => $proof_of_identity, ]);
        }

        if ($request->get('category_id')) {
            $submission->category()->associate(Category::find($request->get('category_id')));
        }

        if ($request->get('partner_id')) {
            $submission->partner()->associate(Partner::find($request->get('partner_id')));
        }

        if ($request->has('submission_file')) {
            foreach ($request->get('submission_file') as $submission_file_id => $data) {
                $submission_file = SubmissionFile::find($submission_file_id);

                if ($submission_file) {
                    $submission_file->update([
                        'title'       => $data['title'],
                        'description' => $data['description'],
                    ]);
                }
            }
        }

        // Flash the active tab so we can display it on redirect
        session()->flash('active_tab', $request->get('active_tab'));

        if ($submission->update($request->except([ 'profile_image', 'proof_of_identity' ]))) {
            flash()->success('Your submission details have been successfully saved.');
        } else {
            flash()->error('Your submissions details could not be saved at this time. Please try again later.');
        }

        return $request->has('review_application') ? redirect()->route('submission.review', $submission->hash) : back();
    }


    /**
     * Handle submission review.
     *
     * @param Submission $submission
     *
     * @return \Illuminate\View\View
     */
    public function review(Submission $submission)
    {
        $submission->load([
            'user',
            'category',
            'partner',
            'files',
            'verifications' => function ($query) {
                $query->with('user')->latest();
            },
        ]);

        $submission_files = $submission->files->groupBy('file_group');

        return view('submission.review')->with([
            'submission'        => $submission,
            'review'            => true,
            'has_action'        => false,
            'active_tab'        => 'details',
            'partners_disabled' => true,
            'submission_files'  => $submission_files,
        ]);
    }


    /**
     * Handle a user submitting their application for review.
     *
     * @param Submission $submission
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function submit(Submission $submission)
    {
        $submission->fill([ 'submitted_at' => new \DateTime, ]);

        if ($submission->status()->associate(Status::find(Status::SUBMITTED))->save()) {
            flash()->success('Your application has been successfully submitted. Good luck!');

            event(new ApplicationWasSubmitted($submission));
        } else {
            flash()->error('Your appliation could not be submitted. Please try again later.');
        }

        return redirect()->route('submission.review', $submission->hash);
    }


    /**
     * Verify a submitted application.
     *
     * @param VerifyApplicationRequest $request
     * @param Submission               $submission
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verify(VerifyApplicationRequest $request, Submission $submission)
    {
        if ($request->has('allow_resubmission')) {
            $status = Status::find(Status::PENDING);
        } else {
            $status = Status::find($request->get('accepted') ? Status::APPROVED : Status::REJECTED);
        }

        // New up a verification
        $verification = new Verification([
            'accepted'     => $request->get('accepted'),
            'reason'       => $request->get('reject_reason'),
            'can_resubmit' => $request->has('allow_resubmission'),
        ]);

        // Associate the logged in user with the verification
        $verification->user()->associate(auth()->user());

        // Attach the verification to the submission
        $submission->verifications()->save($verification);

        // Associate the status with the submission
        $submission->status()->associate($status);
        $submission->save();

        event(new ApplicationWasVerified(
            $submission,
            $request->get('accepted'),
            nl2br($request->get('reject_reason')),
            $request->has('allow_resubmission')
        ));

        flash()->success(sprintf('The application was successfully %s.', $request->get('accepted') ? 'accepted' : 'rejected'));

        return redirect()->route('submission.review', $submission->hash);
    }
}

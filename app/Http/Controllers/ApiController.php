<?php

namespace Pda\Http\Controllers;

use Illuminate\Http\Request;
use Pda\Entities\Category;
use Pda\Http\Controllers\Controller;
use Pda\IdentifiesSubmissionFileRequirements;

/**
 * API Controller
 *
 * @package    Pda
 * @subpackage Http\Controllers
*/
class ApiController extends Controller
{
    use IdentifiesSubmissionFileRequirements;

    /**
     * Return a list of active category partners.
     *
     * @param  Request $request Request object
     *
     * @return array
     */
    public function availableCategoryPartners(Request $request)
    {
        return array_merge(
            [ [ 'id' => 0, 'name' => 'Select partner organisation', ], ],
            Category::availablePartners($request->get('category_id'))->get([ 'id', 'name', ])->toArray()
        );
    }


    /**
     * Return submission file requirements for the given category.
     *
     * @param  Request $request Request instance
     * @return array
     */
    public function submissionFileRequirements(Request $request)
    {
        return $this->categoryFileRequirements($request->get('category_id'));
    }


    /**
     * Retrieve all partners associated with the given category.
     *
     * @param  Request $request Request object
     * @return array
     */
    public function allCategoryPartners(Request $request)
    {
        return Category::find($request->get('category_id'))->partners()->lists('name', 'id');
    }
}

<?php

namespace Pda\Http\Controllers;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Pda\Entities\Role;

/**
 * Base application controller
 *
 * @package    Pda
 * @subpackage Http\Controllers
*/
abstract class Controller extends BaseController
{
    use DispatchesCommands, ValidatesRequests;

    /**
     * @var Guard
     */
    protected $authUser;


    /**
     * Class constructor.
     *
     * @param Guard $auth Guard instance
     */
    public function __construct(Guard $auth)
    {
        $this->authUser = $auth->user();
    }


    /**
     * Determine the login redirect path based on the user's role
     *
     * @return string
     */
    protected function getRedirectPath(Guard $auth)
    {
        if (! $auth->user()) return $this->redirectPath();

        switch ($auth->user()->roles()->first()->id) {
            case Role::STAFF:
            case Role::ADMIN:
                return route('admin.applications.index');
                break;
            case Role::PARTNER:
                return route('admin.applications.accepted.index');
                break;
            case Role::APPLICANT:
                return route('my.submission');
                break;
            case Role::JUDGE:
                return route('admin.applications.shortlisted.index');
                break;
            default:
                return $this->redirectPath();
                break;
        }
    }
}

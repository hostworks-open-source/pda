<?php

namespace Pda\Http\Controllers;

use Pda\Http\Requests;
use Illuminate\Http\Request;
use Pda\Entities\Assessment;
use Pda\Entities\Submission;
use Pda\Entities\AssessmentGroup;
use Illuminate\Contracts\Auth\Guard;
use Pda\Events\AssessmentWasCreated;
use Pda\Http\Controllers\Controller;
use Pda\Events\AssessmentWasCreatedOrDeleted;

/**
 * Submission assessment controller
 *
 * @package    Pda
 * @subpackage Http\Controllers
*/
class AssessmentController extends Controller
{
    /**
     * Store assessment data for the given submission.
     *
     * @param  Request    $request    Request instance
     * @param  Submission $submission Submission instance
     * @return \Illuminate\Http\RedirectResponse
     */
    public function assess(Request $request, Submission $submission)
    {
        $assessmentGroup = AssessmentGroup::find($request->get('assessment_group_id'));

        // Remove all existing assessments for this submission submitted by this user, first.
        Assessment::where('submission_id', $submission->id)
            ->where('user_id', $this->authUser->id)
            ->where('assessment_group_id', $assessmentGroup->id)
            ->delete();

        $assessment = Assessment::create($request->only([
            'submission_files', 'achievements', 'current_engagements', 'ambitions_plan_of_action', 'apra_career_advancement',
        ]));

        // Attach the assessment to the submission
        $submission->assessments()->save($assessment);

        // Attach the assessment to the user
        $this->authUser->assessments()->save($assessment);

        // Associate the assessment to the relevant group
        $assessment->group()->associate($assessmentGroup)->save();

        event(new AssessmentWasCreatedOrDeleted($submission, $assessmentGroup));

        flash()->success('Your assessment has been submitted!');

        return back();
    }
}

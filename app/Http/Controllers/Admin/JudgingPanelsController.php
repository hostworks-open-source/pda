<?php

namespace Pda\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Pda\Entities\AssessmentGroup;
use Pda\Entities\Category;
use Pda\Entities\Panel;
use Pda\Entities\Role;
use Pda\Entities\Status;
use Pda\Entities\Submission;
use Pda\Entities\User;
use Pda\Http\Controllers\Controller;
use Pda\Http\Requests;
use Pda\Http\Requests\UpdatePanelRequest;

class JudgingPanelsController extends Controller
{
    /**
     * Display all judging panels.
     *
     * @param  Panel  $panel Panel model instance
     *
     * @return \Illuminate\View\View
     */
    public function index(Panel $panel)
    {
        $panels = Panel::with('judges', 'category', 'applications')->get();

        return view('admin.judging.panels.index', compact('panels'));
    }


    /**
     * Edit the given judging panel.
     *
     * @param  Panel    $panel    Panel model instance
     *
     * @return \Illuminate\View\View
     */
    public function edit(Panel $panel)
    {
        $panel->load('category', 'judges', 'applications', 'applications.user');

        $categories = Category::lists('name', 'id');
        $groups     = AssessmentGroup::lists('name', 'id');
        $judges     = User::selectRaw('id, CONCAT_WS(" ", name_first, name_last) as judgeName')
                          ->ofType(Role::JUDGE)
                          ->lists('judgeName', 'id');

        return view('admin.judging.panels.edit', compact('panel', 'categories', 'groups', 'judges'));
    }


    /**
     * Update an existing judging panel.
     *
     * @param  UpdatePanelRequest $request Update panel form request
     * @param  Panel              $panel   Panel to be updated
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdatePanelRequest $request, Panel $panel)
    {
        $panel->fill($request->only([ 'name', 'max_applicants', ]));
        $panel->category()->associate(Category::find($request->get('category_id')));
        $panel->assessmentGroup()->associate(AssessmentGroup::find($request->get('assessment_group_id')));

        if ($panel->save()) {
            $panel->judges()->sync($request->get('judges'));

            flash()->success('Successfully updated judging panel!');
        } else {
            flash()->error('Could not update judging panel at this time. Please try again later.');
        }

        return redirect()->back();
    }


    /**
     * Detach a submission from the given panel.
     *
     * @param  Panel      $panel      Panel instance
     * @param  Submission $submission Submission instance
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function detachSubmission(Panel $panel, Submission $submission)
    {
        $panel->applications()->detach($submission->id);

        if ($panel->save()) {
            flash()->success('Successfully detached submission from panel.');
        } else {
            flash()->error('Could not detach the specififed submission from the panel.');
        }

        return redirect()->back();
    }


    /**
     * List all valid applications, which can be attached to a judging panel.
     *
     * @param  Panel  $panel Panel model instance
     *
     * @return \Illuminate\View\View
     */
    public function attachSubmissionIndex(Panel $panel)
    {
        $panel->load('category');

        // Find all submissions for this panel's assigned category that are already approved,
        // have completed their submission, have been ranked by a partner organisation by
        // a partner organisation and are not already assigned to this judging panel.
        $submissions = $panel->category
                             ->submissions()
                             ->canBeAssignedToPanel($panel)
                             ->get();

        return view('admin.judging.panels.attach', compact('panel', 'submissions'));
    }


    /**
     * Attach the given submission to the specified panel for judging.
     *
     * @param  Panel      $panel      Panel model instance.
     * @param  Submission $submission Submission model instance.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function attachSubmission(Panel $panel, Submission $submission)
    {
        if ($panel->slotsAvailable() == 0) {
            flash()->error('This panel is already full. Please detach applicants or increase the maximum number of applicants.');

            return back();
        }

        $panel->applications()->attach($submission->id);

        if ($panel->save()) {
            flash()->success('Successfully attached application to this panel!');
        } else {
            flash()->error('Could not attach application to this panel. Please try again later.');
        }

        return back();
    }
}

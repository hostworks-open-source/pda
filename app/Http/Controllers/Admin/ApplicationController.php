<?php

namespace Pda\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Pda\Entities\Category;
use Pda\Entities\Partner;
use Pda\Entities\Status;
use Pda\Entities\Submission;
use Pda\Http\Controllers\Controller;

/**
 * Admin applications controller
 *
 * @package    Pda
 * @subpackage Http\Controllers\Admin
*/
class ApplicationController extends Controller
{
    /**
     * Display a list of all applications.
     *
     * @param Request $request
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // Get the applicable applications
        $applications = Submission::filter($request)->sort($request)
            ->with('user', 'category', 'partner', 'status', 'latestVerification', 'latestVerification.user')
            ->paginate(50);

        // Get all the submission statuses in the database
        $statuses = [ '' => 'Status', 0 => 'All', ] + Status::where('type', 'submission')->orderBy('name')->lists('name', 'id');

        // Get all the categories in the database
        $categories = [ '' => 'Category', 0 => 'All', ] + Category::orderBy('name')->lists('name', 'id');

        // Get all the partners in the database
        $partners = [ '' => 'Partner', 0 => 'All', ] + Partner::orderBy('name')->lists('name', 'id');

        return view('admin.applications', compact('statuses', 'applications', 'categories', 'partners', 'request'));
    }
}

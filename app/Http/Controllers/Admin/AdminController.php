<?php

namespace Pda\Http\Controllers\Admin;

use Pda\Http\Controllers\Controller;

/**
 * Admin controller
 *
 * @package    Pda
 * @subpackage Http\Controllers
*/
class AdminController extends Controller
{
    /**
     * Admin index.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('admin.index');
    }
}

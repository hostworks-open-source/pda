<?php

namespace Pda\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Pda\Entities\Category;
use Pda\Entities\Partner;
use Pda\Http\Controllers\Controller;
use Pda\Http\Requests;
use Pda\Http\Requests\UpdateCategoryRequest;

/**
 * Admin category controller
 *
 * @package    Pda
 * @subpackage Http\Controllers\Admin
*/
class CategoryController extends Controller
{
    /**
     * Display all categories.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $categories = Category::with('submissions', 'partners')->get();
        $partners   = Partner::all()->lists('name', 'id');

        return view('admin.categories', compact('categories', 'partners'));
    }


    /**
     * Update an existing category.
     *
     * @param  UpdateCategoryRequest $request Request object
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCategoryRequest $request)
    {
        $category = Category::find($request->get('category_id'));

        if (! $category) {
            flash()->error('Could not locate the specified category');

            return redirect()->route('admin.categories');
        }

        $category->name = $request->get('name');
        $category->save();

        if ($request->has('partners')) {
            $category->partners()->sync($request->get('partners'));
        }

        flash()->success('Successfully updated category ' . $category->name);

        return redirect()->route('admin.categories.index');
    }
}

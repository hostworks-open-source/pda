<?php

namespace Pda\Http\Controllers\Admin;

use Pda\Http\Requests;
use Pda\Entities\Status;
use Pda\Entities\Category;
use Illuminate\Http\Request;
use Pda\Entities\Assessment;
use Pda\Entities\Submission;
use Pda\Entities\AssessmentGroup;
use Illuminate\Contracts\Auth\Guard;
use Pda\Http\Controllers\Controller;
use Pda\Events\AssessmentWasCreatedOrDeleted;

/**
 * Partner controller
 *
 * @package    Pda
 * @subpackage Http\Controllers\Admin
*/
class PartnerController extends Controller
{
    /**
     * Display a list of approved approved applications.
     *
     * @param  \Illuminate\Http\Request $request Request object
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if (! $this->authUser->partner) {
            flash('Only partners may access the requested page.');

            return redirect()->route('home');
        }

        // Get the categories this user's partner organisation is eligible to rank
        $partnerCategories = $this->authUser->partner->categories->lists('id');

        // Get all applications that are yet to be assessed by this user
        $applications = Submission::with('user', 'category', 'partner', 'latestVerification.user')
            ->assessable(AssessmentGroup::PARTNER)
            ->where('status_id', Status::APPROVED)
            ->whereIn('category_id', $partnerCategories)
            ->where('partner_id', $this->authUser->partner->id)
            ->paginate(50);

        return view('admin.partners.index', compact('applications', 'request'));
    }


    /**
     * Rank the given submission.
     *
     * @param  Submission $submission Submission to rank
     *
     * @return \Illuminate\View\View
     */
    public function rank(Submission $submission)
    {
        $submission->load([
            'user',
            'category',
            'partner',
            'files',
            'verifications' => function ($query) {
                $query->with('user')->latest();
            },
            'userAssessment',
        ]);

        $submission_files = $submission->files->groupBy('file_group');

        return view('submission.review')->with([
            'submission'          => $submission,
            'has_action'          => false,
            'active_tab'          => 'details',
            'submission_files'    => $submission_files,
            'assessing'           => true,
            'assessment_group_id' => AssessmentGroup::PARTNER,
            'maximumScore'        => Assessment::MAXIMUM_SCORE,
        ]);
    }
}

<?php

namespace Pda\Http\Controllers\Admin;

use Pda\Http\Requests;
use Pda\Entities\Panel;
use Illuminate\Http\Request;
use Pda\Entities\Assessment;
use Pda\Entities\Submission;
use Pda\Entities\AssessmentGroup;
use Pda\Http\Controllers\Controller;
use Pda\Events\AssessmentWasCreatedOrDeleted;

/**
 * Judge controller
 *
 * @package    Pda
 * @subpackage Http\Controllers\Admin
*/
class JudgeController extends Controller
{
    /**
     * Display a list of judging panels.
     *
     * @param  \Illuminate\Http\Request $request Request instance
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if (! $this->authUser->hasRole('Judge')) {
            flash('Only judges may access the requested page.');

            return redirect()->route('home');
        }

        // If the logged in user belongs to a single panel, redirect them immediately
        if ($this->authUser->panels->count() == 1) {
            return redirect()->route('admin.applications.shortlisted.show', $this->authUser->panels->first()->id);
        }

        // Iterate through each of the assigned panels and load the number of assessable applications
        $this->authUser->panels->each(function ($panel) {
            $panel->assessable_count = $panel->applications()->assessable($panel->assessment_group_id)->count();
        });

        return view('admin.judging.index', [ 'panels' => $this->authUser->panels, ]);
    }


    /**
     * Show assessable applications for the given panel.
     *
     * @param  Request $request Request instance
     * @param  Panel   $panel   Panel model instance
     *
     * @return \Illuminate\View\View
     */
    public function show(Request $request, Panel $panel)
    {
        $panel->load('category');

        if (! in_array($panel->id, $this->authUser->panels->lists('id'))) {
            flash('You do not have permission to access the requested judging panel.');

            return redirect()->route('home');
        }

        $applications = $panel->applications()
                              ->with('category', 'partner', 'user')
                              ->assessable($panel->assessment_group_id)
                              ->paginate(50);

        return view('admin.judging.show', compact('panel', 'applications', 'request'));
    }


    /**
     * Show judges applications that have already been ranked,
     * such that they might be removed to be ranked again.
     *
     * @param  Request $request
     * @param  Panel   $panel
     *
     * @return \Illuminate\View\View
     */
    public function completed(Request $request, Panel $panel)
    {
        $panel->load('category');

        if (! in_array($panel->id, $this->authUser->panels->lists('id'))) {
            flash('You do not have permission to access the requested judging panel.');

            return redirect()->route('home');
        }

        $that = $this;
        $applications = $panel->applications()->with([ 'assessments' => function ($query) use ($that, $panel) {
            $query->where('user_id', $that->authUser->id)
                  ->where('assessment_group_id', $panel->assessment_group_id);
        }, ])->assessed($panel->assessment_group_id)->paginate(50);

        return view('admin.judging.show', compact('panel', 'applications', 'request'));
    }


    /**
     * Rank the given submission.
     *
     * @param  Submission $submission Submission to rank
     * @param  Panel      $panel      Panel for which we are ranking against
     *
     * @return \Illuminate\View\View
     */
    public function rank(Submission $submission, Panel $panel)
    {
        if (! $submission->panels->contains('id', $panel->id)) {
            flash('The requested submission is not allocated to your judging panel');

            return redirect()->route('home');
        }

        $submission->load([
            'user',
            'category',
            'partner',
            'files',
            'verifications' => function ($query) {
                $query->with('user')->latest();
            },
            'userAssessment' => function ($query) use ($panel) {
                $query->where('assessment_group_id', $panel->assessment_group_id);
            },
        ]);

        $submission_files = $submission->files->groupBy('file_group');

        return view('submission.review')->with([
            'submission'          => $submission,
            'has_action'          => false,
            'active_tab'          => 'details',
            'submission_files'    => $submission_files,
            'assessing'           => true,
            'assessment_group_id' => $panel->assessment_group_id,
            'maximumScore'        => Assessment::MAXIMUM_SCORE,
        ]);
    }


    /**
     * Remove assessment for an existing submission, for this user.
     *
     * @param  Submission $submission
     * @param  Panel      $panel
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function remove(Submission $submission, Panel $panel)
    {
        $parent = $this;

        $submission->assessments->each(function ($assessment) use ($parent, $panel) {
            if (( $assessment->user_id == $parent->authUser->id ) && ( $assessment->assessment_group_id == $panel->assessment_group_id )) {
                $assessment->delete();
            }
        });

        event(new AssessmentWasCreatedOrDeleted($submission, AssessmentGroup::find($panel->assessment_group_id)));

        flash('You may now submit a new assessment for this submission');

        return redirect()->route('submission.rank.judge', [ $submission->hash, $panel->id, ]);
    }
}

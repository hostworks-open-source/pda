<?php

namespace Pda\Http;

use Illuminate\Http\Response;

/**
 * Generic JSON response methods
 *
 * @package    Pda
 * @subpackage Http
*/
trait JsonResponse
{
    /**
     * HTTP status code to return.
     *
     * @var int
     */
    private $status_code = Response::HTTP_OK;

    /**
     * Data to return.
     *
     * @var array
     */
    private $data = [ ];

    /**
     * Message to return.
     *
     * @var null|string
     */
    private $message = null;

    /**
     * Whether or not the request succeeded.
     *
     * @var boolean
     */
    private $success = true;


    /**
     * Set the message property.
     *
     * @param $message
     *
     * @return $this
     */
    protected function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }


    /**
     * Set the data property.
     *
     * @param array $data
     *
     * @return $this
     */
    protected function setData(array $data = [ ])
    {
        $this->data = $data;

        return $this;
    }


    /**
     * Mark the operation as successful.
     *
     * @return $this
     */
    protected function succeeded()
    {
        $this->success = true;

        return $this;
    }


    /**
     * Mark the operation as failed.
     *
     * @return $this
     */
    protected function failed()
    {
        $this->success = false;

        return $this;
    }


    /**
     * Set the status code property.
     *
     * @param $status_code
     *
     * @return $this
     */
    protected function setStatusCode($status_code)
    {
        $this->status_code = $status_code;

        return $this;
    }


    /**
     * Entity not found.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respondNotFound()
    {
        return $this->failed()
            ->setStatusCode(Response::HTTP_NOT_FOUND)
            ->respond();
    }


    /**
     * Everything is OK.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respondOk()
    {
        return $this->succeeded()
            ->setStatusCode(Response::HTTP_OK)
            ->respond();
    }


    /**
     * The request was bad.
     *
     * @return $this
     */
    public function respondBadRequest()
    {
        return $this->failed()
            ->setStatusCode(Response::HTTP_BAD_REQUEST)
            ->respond();
    }


    /**
     * User not authorised.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respondNotAuthorised()
    {
        return $this->failed()
            ->setStatusCode(Response::HTTP_UNAUTHORIZED)
            ->respond();
    }


    /**
     * Request not acceptable.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respondNotAcceptable()
    {
        return $this->failed()
            ->setStatusCode(Response::HTTP_NOT_ACCEPTABLE)
            ->respond();
    }


    /**
     * Request entity too large.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respondTooLarge()
    {
        return $this->failed()
            ->setStatusCode(Response::HTTP_REQUEST_ENTITY_TOO_LARGE)
            ->respond();
    }


    /**
     * The request was unprocessable.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respondUnprocessable()
    {
        return $this->failed()
            ->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->respond();
    }


    /**
     * Return a JSON response.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function respond()
    {
        $response = [
            'data'    => $this->data,
            'success' => $this->success,
        ];

        if ($this->success) {
            $response['message'] = $this->message;
        } else {
            $response['error'] = $this->message;
        }

        return response()->json($response, $this->status_code);
    }
}

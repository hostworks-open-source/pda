<?php

namespace Pda\Exceptions;

use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Allow us to differentiate between a generic model not found and a submission not found
 *
 * @package    Pda
 * @subpackage Exceptions
*/
class SubmissionNotFoundException extends ModelNotFoundException
{
}

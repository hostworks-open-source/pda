<?php

return [
    'one'   => 'Details of your achievements in composing or songwriting.',
    'two'   => 'Details of your current engagement in musical studies, songwriting, performing, recording, etc.',
    'three' => 'What are your ambitions for a future career in composing and songwriting and how do you propose to achieve those ambitions?',
    'four'  => 'How will winning an APRA Professional Development Award help you achieve your goal?',
];

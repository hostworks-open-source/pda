@extends('layouts.master')

@section('content')
    @include('flash::message')

    <div class="row">
	    <div class="col-md-9">
	        <h1>2015 APRA Professional Development Awards</h1>
            @if(! auth()->guest())
                @if(auth()->user()->hasRole('Admin', 'Staff'))
                    <a href="{{ route('admin.index') }}" class="btn btn-lg btn-primary">Staff area</a>
                @elseif(auth()->user()->hasRole('Partner'))
                    <a href="{{ route('admin.applications.accepted.index') }}" class="btn btn-lg btn-primary">Assess approved applicants</a>
                @elseif(auth()->user()->hasRole('Judge'))
                    <a href="{{ route('admin.applications.shortlisted.index') }}" class="btn btn-lg btn-primary">Assess shortlisted applicants</a>
                @endif
            @elseif(auth()->guest() || auth()->user()->hasRole('Applicant'))
                <a href="{{ route('submission.create') }}" class="btn btn-lg btn-primary">{{ auth()->user() && auth()->user()->submission ? 'Resume' : 'Submit' }} your application</a>
            @endif

	        @if(auth()->guest()) or {!! link_to('auth/login', 'login') !!} @endif
	    </div>
    </div>
    <div class="row about-app">
    	<div class="col-md-4">
    		<h3>Browser Information</h3>
    		<p>Before you commence your application, ensure you have the <span class="emphasis">latest version</span> of your internet browser.</p>
    		<p>You must have JavaScript <span class="emphasis">enabled</span> to use this application, which most browsers have enabled by default.</p>
    		@if($browserVersion) <p>Your current browser is <span class="emphasis">{{ $browserVersion }}</span>.</p> @endif
		</div>
    </div>
@stop

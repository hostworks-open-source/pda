@extends('layouts.admin')

@section('page_heading', 'Category management') @stop

@section('content')
	<table class="table table-condensed table-striped">
	<thead>
	<tr>
		<th>Category Name</th>
		<th>Applications</th>
		<th>Partner Organisations</th>
		<th>Actions</th>
	</tr>
	</thead>
	<tbody>
		@each('admin.partials.category_row', $categories, 'category', 'raw|<tr><td colspan="4">No categories at this time</td></tr>')
	</tbody>
	</table>

	@include('partials._modal', [ 'modal_id' => 'updateCategory', 'modal_title' => 'Edit Category', ])
@stop

@section('footer_scripts')
	@parent

	<script>
		$(function() {
			$('.update-category').click(function () {
				var modalBody 	  = $('#updateCategory .modal-body'),
					category_id   = $(this).data('category'),
					category_name = $(this).data('categoryname');

				modalBody.empty();
				modalBody.append(
					'{!! Form::open([ "route" => [ "admin.categories.update", ], "class" => "form-horizontal", "id" => "form-category-update", "method" => "post", "role" => "form", ]) !!}' +
					'<input name="category_id" type="hidden" value="' + category_id + '">' +
					'<div class="form-group">' +
					'{!! Form::label("name", "Category Name", [ "class" => "control-label", ]) !!}' +
					'<input id="name" name="name" class="form-control" value="' + category_name + '">' +
					'</div>' +
					'<div class="form-group">' +
					'{!! Form::label("partners", "Supporting Partner Organisations", [ "class" => "control-label", ]) !!}' +
					'{!! Form::select("partners[]", $partners, null, [ "class" => "form-control", "id" => "category-partners", "multiple", ]) !!}' +
					'</div>' +
					'{!! Form::close() !!}'
				);

				$('#updateCategory .save-changes').click(function (e) {
					e.preventDefault();
					e.stopPropagation();

					$('#form-category-update').submit();
				});

				$.ajax({
					method: 'GET',
					url: '{{ route('api.category.partnersAll') }}',
					dataType: 'json',
					data: {
						category_id: category_id
					},
					success: function (response) {
						$.each(response, function (key, value) {
							modalBody.find('#category-partners option[value="' + key + '"]').prop('selected', true);
						});
					}
				});
			});
		});
	</script>
@stop
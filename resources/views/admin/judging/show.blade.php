@extends('layouts.admin')

@section('page_heading')
    @if(! Route::is('admin.applications.shortlisted.show.completed'))
        {{ $panel->category->name }} - {{ $panel->name }} - Applications to be ranked ({{ $applications->count() }})
    @else
        {{ $panel->category->name }} - {{ $panel->name }} - Applications already ranked ({{ $applications->count() }})
    @endif
@stop

@section('content')
    @if(time() > strtotime(env('JUDGING_CLOSE_DATE_JUDGES')))
        <div class="alert alert-info">Judging PDA applications closed on {{ date('l jS F, Y \a\t g:ia', strtotime(env('JUDGING_CLOSE_DATE_JUDGES'))) }}</div>
    @else
        <ul class="nav nav-pills">
            <li role="presentation" @if(! Route::is('admin.applications.shortlisted.show.completed')) class="active" @endif><a href="{{ route('admin.applications.shortlisted.show', $panel) }}">Applications to be ranked</a></li>
            <li role="presentation" @if(Route::is('admin.applications.shortlisted.show.completed')) class="active" @endif><a href="{{ route('admin.applications.shortlisted.show.completed', $panel) }}">Applications already ranked</a></li>
        </ul>

        <table class="table table-condensed table-striped">
        @include('admin.partials.application_table_header')
        <tbody>
            @each('admin.partials.application_row', $applications, 'application', 'raw|<tr><td colspan="9">No applications at this time</td></tr>')
        </tbody>
        </table>

        {!! $applications->appends([
            'sort'  => $request->get('sort'),
            'order' => $request->get('order'),
        ])->render() !!}
    @endif
@stop
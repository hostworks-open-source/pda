@extends('layouts.admin')

@section('page_heading')
    Your judging panels ({{ $panels->count() }})
@stop

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <table class="table table-condensed table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Applications</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @if($panels->count() == 0)
            <tr>
                <td colspan="3">You are not assigned to any panels at this time.</td>
            </tr>
            @else
            @foreach($panels as $panel)
            <tr>
                <td>{{ $panel->category->name }} - {{ $panel->name }}, {{ $panel->assessmentGroup->name }}</td>
                <td>{{ $panel->assessable_count }}</td>
                <td><a href="{{ route('admin.applications.shortlisted.show', $panel->id) }}">View Applications</a></td>
            </tr>
            @endforeach
            @endif
            </tbody>
            </table>
        </div>
    </div>
@stop
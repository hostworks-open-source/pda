@extends('layouts.admin')

@section('page_heading')
    Attach applications to {{ $panel->category->name }} - {{ $panel->name }}
    {!! link_to_route('admin.judging.panels.edit', 'Edit panel', [ $panel->id, ], [ 'class' => 'btn btn-xs btn-info', ]) !!}<br />
    <small>{{ $panel->applications->count() }} / {{ $panel->max_applicants }} applications already attached.</small>
@stop

@section('content')
    <table class="table table-condensed table-striped">
    <thead>
    <tr>
        <th>Applicant Name</th>
        <th>Partner Rank</th>
        @if($panel->assessment_group_id == Pda\Entities\AssessmentGroup::ROUND_TWO)<th>Round 1 Rank</th>@endif
        <th>Partner Organisation</th>
        <th>Submitted</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @if($submissions->isEmpty())
    <tr>
        <td colspan="5">No submissions available at this time</td>
    </tr>
    @else
        @foreach($submissions as $submission)
        <tr>
            <td>{{ $submission->user->name }}</td>
            <td>{{ $submission->partner_organisation_rank }}</td>
            @if($panel->assessment_group_id == Pda\Entities\AssessmentGroup::ROUND_TWO) <td>{{ $submission->round_one_rank }}</td>@endif
            <td>{{ $submission->partner->name }}</td>
            <td>{{ $submission->submitted_at ? $submission->submitted_at->format('d/m/Y') : 'N/A' }}</td>
            <td>
                {!! link_to_route('submission.review', 'Review', [ $submission->hash, ], [ 'target' => '_blank', ]) !!}<span class="text-muted"> / </span>
                {!! link_to_route('admin.judging.panels.attachSubmission', 'Attach to panel', [ $panel->id, $submission->hash, ]) !!}
            </td>
        </tr>
        @endforeach
    @endif
    </tbody>
    </table>
@stop
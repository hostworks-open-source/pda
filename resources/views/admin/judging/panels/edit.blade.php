@extends('layouts.admin')

@section('page_heading')
    Editing {{ $panel->category->name }} - {{ $panel->name }}
    {!! link_to_route('admin.judging.panels.attachSubmissionIndex', sprintf('Add applications (%d eligible)', $panel->category->submissions()->canBeAssignedToPanel($panel)->count()), [ $panel->id, ], [ 'class' => 'btn btn-xs btn-info', ]) !!}
@stop

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Update judging panel</h3>
                </div>
                <div class="panel-body">
                    {!! Form::model($panel, [ 'route' => [ 'admin.judging.panels.update', $panel->id, ], 'class' => 'form', 'method' => 'post', 'role' => 'form', ]) !!}
                        <div class="form-group @if($errors->has('category_id')) has-error @endif">
                            {!! Form::label('category_id', 'Category') !!}
                            {!! Form::select('category_id', $categories, null, [ 'class' => 'form-control', 'id' => 'category_id', ]) !!}
                            {!! $errors->first('category_id', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group @if($errors->has('assessment_group_id')) has-error @endif">
                            {!! Form::label('assessment_group_id', 'Assessment Group') !!}
                            {!! Form::select('assessment_group_id', $groups, null, [ 'class' => 'form-control', 'id' => 'assessment_group_id', ]) !!}
                            {!! $errors->first('assessment_group_id', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group @if($errors->has('name')) has-error @endif">
                            {!! Form::label('name', 'Panel name') !!}
                            {!! Form::text('name', null, [ 'class' => 'form-control', 'id' => 'name', 'placeholder' => 'Panel name', ]) !!}
                            {!! $errors->first('name', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group @if($errors->has('max_applicants')) has-error @endif">
                            {!! Form::label('max_applicants', 'Maximum Applicants') !!}
                            {!! Form::number('max_applicants', null, [ 'class' => 'form-control', 'id' => 'max_applicants', 'placeholder' => 'Maximum Applicants', ]) !!}
                            {!! $errors->first('max_applicants', '<span class="help-block">:message</span>') !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('judges', 'Select Judges') !!}
                            {!! Form::select('judges[]', $judges, $panel->judges->lists('id'), [ 'class' => 'form-control', 'id' => 'judges', 'multiple', 'size' => 10, ]) !!}
                        </div>
                        {!! Form::submit('Update', [ 'class' => 'btn btn-primary', ]) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Panel applicants</h3>
                </div>
                <table class="table table-condensed table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Submitted</th>
                    <th>P. Rank</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if($panel->applications->isEmpty())
                <tr>
                    <td colspan="4">No applications currently attached to this panel</td>
                </tr>
                @else
                    @foreach($panel->applications as $application)
                    <tr>
                        <td>{{ $application->user->name }}</td>
                        <td>{{ $application->submitted_at ? $application->submitted_at->format('d/m/Y') : 'N/A' }}</td>
                        <td>{{ $application->partner_organisation_rank }}</td>
                        <td>
                            {!! link_to_route('submission.review', 'Review', [ $application->hash, ], [ 'target' => '_blank', ]) !!}<span class="text-muted"> / </span>
                            {!! link_to_route('admin.judging.panels.detachSubmission', 'Remove from panel', [ $panel->id, $application->hash, ]) !!}
                        </td>
                    </tr>
                    @endforeach
                @endif
                </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
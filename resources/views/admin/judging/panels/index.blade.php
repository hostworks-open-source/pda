@extends('layouts.admin')

@section('page_heading', 'Judging Panels') @stop

@section('content')
    <table class="table table-condensed table-striped">
    <thead>
    <tr>
        <th>Category</th>
        <th>Name</th>
        <th>Group</th>
        <th>Judges</th>
        <th>Applicants</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    @foreach($panels as $panel)
    <tr>
        <td>{{ $panel->category->name }}</td>
        <td>{{ $panel->name }}</td>
        <td>{{ $panel->assessmentGroup->name }}</td>
        <td>{{ $panel->judges->count() }}</td>
        <td>{{ $panel->applications->count() }} / {{ $panel->max_applicants }} ({{ $panel->slotsAvailable() }} slots available, {{ $panel->category->submissions()->canBeAssignedToPanel($panel)->count() }} eligible)</td>
        <td>
            {!! link_to_route('admin.judging.panels.edit', 'Edit', [ $panel->id, ]) !!}<span class="text-muted"> / </span>
            {!! link_to_route('admin.judging.panels.attachSubmissionIndex', 'Add applications', [ $panel->id, ]) !!}
        </td>
    </tr>
    @endforeach
    </tbody>
    </table>
@stop

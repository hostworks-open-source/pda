@if(auth()->user()->hasRole('Staff'))
	<div class="container">
	    <div class="row">
	        <div class="col-md-12">
	            <ul class="nav nav-pills">
	                <li role="presentation" @if(Route::is('admin.applications.*')) class="active" @endif>{!! link_to_route('admin.applications.index', 'Applications') !!}</li>
	                <li role="presentation" @if(Route::is('admin.categories.index')) class="active" @endif>{!! link_to_route('admin.categories.index', 'Categories') !!}</li>
	                <li role="presentation" @if(Route::is('admin.judging.*')) class="active" @endif>{!! link_to_route('admin.judging.panels.index', 'Juding Panels') !!}</li>
	            </ul>
	        </div>
	    </div>
	</div>
@endif
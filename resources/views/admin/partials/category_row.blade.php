<tr>
	<td>{{ $category->name }}</td>
	<td>{{ $category->submissions->count() }}</td>
	<td>{{ $category->partners->count() }}</td>
	<td>
		<a href="#" class="update-category" data-toggle="modal" data-target="#updateCategory" data-category="{{ $category->id }}" data-categoryName="{{ $category->name }}">Edit</a>
	</td>
</tr>
<tr>
    <td>{{ $application->user->name }}</td>
    <td>{{ $application->performer_name }}</td>
    @if(auth()->user()->hasRole('Staff')) <td>{{ $application->user->email }}</td> @endif
    <td>{{ $application->category ? $application->category->name : 'Not set' }}</td>
    <td>{{ $application->partner ? $application->partner->name : 'Not set' }}</td>
    @if(auth()->user()->hasRole('Staff')) <td>{{ $application->submitted_at ? $application->submitted_at->format('d/m/Y') : 'N/A' }}</td> @endif
    @if(auth()->user()->hasRole('Staff')) <td>{{ $application->partner_organisation_rank }}</td> @endif
    @if(auth()->user()->hasRole('Staff')) <td>{{ $application->round_one_rank }}</td> @endif
    @if(auth()->user()->hasRole('Staff')) <td>{{ $application->round_two_rank }}</td> @endif
    @if(auth()->user()->hasRole('Judge') && Route::is('admin.applications.shortlisted.show.completed')) <td>{{ $application->assessments->first()->total_score }}</td> @endif
    @if(auth()->user()->hasRole('Staff'))
    <td>
        @if( ! $application->latestVerification)
            N/A
        @else
            {{ $application->latestVerification->user->name }}
        @endif
    </td>
    @endif
    <td>
        @if(auth()->user()->hasRole('Staff', 'Partner') && ! Route::is('*.applications.shortlisted.*'))<a href="{{ route('submission.rank.partner', $application->hash) }}" title="Rank"><i class="fa fa-trophy"></i></a> @endif
        @if(auth()->user()->hasRole('Staff', 'Judge') && Route::is('admin.applications.shortlisted.show'))<a href="{{ route('submission.rank.judge', [ $application->hash, Request::route()->panel->id, ]) }}" title="Judge"><i class="fa fa-star"></i></a> @endif
        @if(auth()->user()->hasRole('Staff', 'Judge') && Route::is('admin.applications.shortlisted.show.completed'))<a href="{{ route('submission.rank.judge.remove', [ $application->hash, Request::route()->panel->id, ]) }}" onclick="javascript: return confirm('Are you sure you want to remove your ranking for this application?');" title="Remove"><i class="fa fa-trash-o"></i></a> @endif
        @if(auth()->user()->hasRole('Staff')) <a href="{{ route('submission.edit', $application->hash) }}" title="Edit"><i class="fa fa-edit"></i></a> @endif
        @if(auth()->user()->hasRole('Staff')) <a href="{{ route('submission.review', $application->hash) }}" title="Review"><i class="fa fa-check-square-o"></i></a> @endif
    </td>
</tr>

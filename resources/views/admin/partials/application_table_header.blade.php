<thead>
<tr>
    <th>@if(auth()->user()->hasRole('Staff')) {!! sort_submissions_by('name', 'Name') !!} @else Name @endif</th>
    <th>@if(auth()->user()->hasRole('Staff')) {!! sort_submissions_by('performer_name', 'Performer Name') !!} @else Performer Name @endif</th>
    @if(auth()->user()->hasRole('Staff')) <th>{!! sort_submissions_by('email', 'Email') !!}</th> @endif
    <th>Category</th>
    <th>Partner</th>
    @if(auth()->user()->hasRole('Staff')) <th>{!! sort_submissions_by('submitted_at', 'Submitted') !!}</th> @endif
    @if(auth()->user()->hasRole('Staff')) <th>{!! sort_submissions_by('partner_organisation_rank', 'P. Rank') !!}</th> @endif
    @if(auth()->user()->hasRole('Staff')) <th>{!! sort_submissions_by('round_one_rank', 'R1 Rank') !!}</th> @endif
    @if(auth()->user()->hasRole('Staff')) <th>{!! sort_submissions_by('round_two_rank', 'R2 Rank') !!}</th> @endif
    @if(auth()->user()->hasRole('Judge') && Route::is('admin.applications.shortlisted.show.completed')) <th>Assessment Rank</th> @endif
    @if(auth()->user()->hasRole('Staff')) <th>Verified</th> @endif
    <th>Actions</th>
</tr>
</thead>
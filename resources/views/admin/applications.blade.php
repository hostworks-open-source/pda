@extends('layouts.admin')

@section('page_heading')
    Professional Development Awards Applications ({{ $applications->total() }})
@stop

@section('content')
    {!! Form::open([ 'route' => 'admin.applications.index', 'class' => 'form-inline', 'method' => 'get', 'role' => 'form', ]) !!}
        <label for="status_id">Filter applications: </label>
        <div class="form-group">
            {!! Form::select('status_id', $statuses, $request->get('status_id'), [ 'class' => 'form-control', 'id' => 'status_id', ]) !!}
        </div>
        <div class="form-group">
            {!! Form::select('category_id', $categories, $request->get('category_id'), [ 'class' => 'form-control', 'id' => 'category_id', ]) !!}
        </div>
        <div class="form-group">
            {!! Form::select('partner_id', $partners, $request->get('partner_id'), [ 'class' => 'form-control', 'id' => 'partner_id', ]) !!}
        </div>
        <div class="form-group">
            {!! Form::text('name', $request->get('name'), [ 'class' => 'form-control', 'id' => 'name', 'placeholder' => 'Name', ]) !!}
        </div>
        <div class="form-group">
            {!! Form::email('email', $request->get('email'), [ 'class' => 'form-control', 'id' => 'name', 'placeholder' => 'Email', ]) !!}
        </div>
        <button class="btn btn-primary" type="submit">Filter</button>
        <a href="{{ route('admin.applications.index') }}" class="btn btn-info">Clear Filter</a>
    {!! Form::close() !!}

    <table class="table table-condensed table-striped">
    @include('admin.partials.application_table_header')
    <tbody>
        @each('admin.partials.application_row', $applications, 'application', 'raw|<tr><td colspan="9">No applications at this time</td></tr>')
    </tbody>
    </table>

    {!! $applications->appends([
        'sort'                      => $request->get('sort'),
        'order'                     => $request->get('order'),
        'name'                      => $request->get('name'),
        'email'                     => $request->get('email'),
        'status_id'                 => $request->get('status_id'),
        'category_id'               => $request->get('category_id'),
        'partner_organisation_rank' => $request->get('partner_organisation_rank'),
        'round_one_rank'            => $request->get('round_one_rank'),
        'round_two_rank'            => $request->get('round_two_rank'),
        'submitted_at'              => $request->get('submitted_at'),
        ])->render() !!}
@stop

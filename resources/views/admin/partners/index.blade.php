@extends('layouts.admin')

@section('page_heading')
	Applications to rank ({{ $applications->total() }})
@stop

@section('content')
	<table class="table table-condensed table-striped">
	@include('admin.partials.application_table_header')
	<tbody>
		@each('admin.partials.application_row', $applications, 'application', 'raw|<tr><td colspan="9">No applications at this time</td></tr>')
	</tbody>
	</table>

	{!! $applications->appends([
        'sort'  => $request->get('sort'),
        'order' => $request->get('order'),
    ])->render() !!}
@stop

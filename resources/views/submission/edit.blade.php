@extends('layouts.master')

@section('content')
    {!! Form::model($submission, [ 'route' => [ 'submission.update', $submission->hash, ], 'class' => 'form', 'id' => 'submission-form', 'files' => true, 'method' => 'put', ]) !!}
        @include('submission.partials.header')

        <div class="clearfix">&nbsp;</div>

        @include('flash::message')

        <div class="row">
            <div class="col-md-12"><span class="required">*</span> denotes required field.</div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content">
                    <div class="clearfix">&nbsp;</div>

                    <div role="tabpanel" class="tab-pane fade @if($active_tab == 'details') in active @endif" id="details">
                         @include('submission.partials.details')
                    </div>
                    <div role="tabpanel" class="tab-pane fade @if($active_tab == 'songs') in active @endif" id="songs">
                        @include('submission.partials.work')
                    </div>
                    <div role="tabpanel" class="tab-pane fade @if($active_tab == 'answers') in active @endif" id="answers">
                        @include('submission.partials.answers')
                    </div>
                    @if(auth()->user()->hasRole('Staff'))
                        <div role="tabpanel" class="tab-pane fade" id="verifications">
                            @include('submission.partials._verifications')
                        </div>
                    @endif
                </div>
            </div>
        </div>
    {!! Form::close() !!}
@stop

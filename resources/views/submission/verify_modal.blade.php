<div class="modal fade" id="modal-verify-application">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Verify Application</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-info">
                    <p>If accepting the application, it will be placed on the shortlist for the first and subsequent rounds of judging.</p>
                    <p>When submitting this form, the applicant will be notified via email whether or not they were submitted.</p>
                </div>
                {!! Form::open([ 'route' => [ 'submission.verify', $submission->hash, ], 'class' => 'form', 'id' => 'form-verify-application', 'method' => 'post', 'role' => 'form', ]) !!}
                    <div class="radio">
                        <label>
                            {!! Form::radio('accepted', 1) !!} Accept application
                        </label>
                        <label>
                            {!! Form::radio('accepted', 0) !!} Reject application
                        </label>
                    </div>
                    <div class="hidden" id="reject-reason">
                        <div class="form-group">
                            {!! Form::label('reject_reason_template', 'Rejection Template') !!}
                            <select name="reject_reason_template" id="reject_reason_template" class="form-control">
                                <option value="">[Choose]</option>
                                <option value="not_individual">Application for a group/band, not individual</option>
                                <option value="corrupt_file">Corrupt files attached</option>
                                <option value="no_video">Film/TV submission, audio files only</option>
                            </select>
                            <span class="help-block">Selection of option will replace any text below.</span>
                        </div>
                        <div class="form-group">
                            {!! Form::label('reject_reason', 'Reason for rejection') !!}
                            {!! Form::textarea('reject_reason', null, [ 'class' => 'form-control', 'id' => 'reject_reason', 'rows' => 10, ]) !!}
                            <span class="help-block">If specified, this reason <em>will</em> be sent to the applicant.</span>
                        </div>
                        <div class="checkbox">
                            <label>
                                {!! Form::checkbox('allow_resubmission', 1) !!} Allow the applicant to resubmit their application
                            </label>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="submit-verification">Submit</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="dropzone work-upload" id="{{ $dropzone_id }}">
    <input name="file_group" class="form-control file_group" type="hidden" value="{{ $dropzone_id }}">
    @if($submission_files->has($dropzone_id))
        <input name="submission_file[{{ $submission_files[$dropzone_id][0]->id }}][title]" class="file-hidden-title" type="hidden" value="{{ $submission_files[$dropzone_id][0]->title }}">
        <input name="submission_file[{{ $submission_files[$dropzone_id][0]->id }}][description]" class="file-hidden-description" type="hidden" value="{{ $submission_files[$dropzone_id][0]->description }}">
    @endif

    <div class="form-group title">
        {!! Form::label('title', 'Title', [ 'class' => 'control-label', ]) !!}
        {!! Form::text('title', $submission_files->has($dropzone_id) ? $submission_files[$dropzone_id][0]->title : null, [ 'class' => 'form-control title', 'placeholder' => 'Work / Song Title', ]) !!}
    </div>
    <div class="form-group description">
        {!! Form::label('description', 'Description', [ 'class' => 'control-label', ]) !!}
        {!! Form::textarea('description', $submission_files->has($dropzone_id) ? $submission_files[$dropzone_id][0]->description : null, [ 'class' => 'form-control description', 'placeholder' => 'Add the description or lyrics', ]) !!}
    </div>
    <div class="dz-message">Click or drop up to two files here to upload</div>
    <div class="dropzone-previews"></div>
</div>

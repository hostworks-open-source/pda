<div class="row">
    <div class="col-md-12">
        <div class="form-group required @if($errors->has('proof_of_identity')) has-error @endif">
            <label for="proof_of_identity" class="control-label">Proof of Identity</label>
            {!! Form::file('proof_of_identity', null, [ 'class' => 'form-control', ]) !!}
            {!! $errors->first('proof_of_identity', '<span class="help-block">:message</span>') !!}
            <span class="help-block">
                @if( ! $submission->proof_of_identity)
                    Please attach a scanned/digital copy of your Drivers License, Passport or photo ID, or a scanned Letter of Authority signed by your parent or guardian if you are under 18.
                @else
                    <img src="{!! $submission->proofOfIdentityLink([ 'w' => 150, ]) !!}" alt="Proof of identify preview" width="150" />
                @endif
            </span>
        </div>
        <div class="form-group required @if($errors->has('biography')) has-error @endif">
            <label for="biography" class="control-label">Biography (<span class="remaining"></span> words remaining)</label>
            <span class="help-block">
                Please provide your biography which may be made available online if you are shortlisted.
            </span>
            {!! $errors->first('biography', '<span class="help-block">:message</span>') !!}
            {!! Form::textarea('biography', null, [ 'class' => 'form-control word-counter', 'data-max-words' => '300', 'rows' => 15, ]) !!}
        </div>
        <div class="form-group required @if($errors->has('achievements')) has-error @endif">
            <label for="achievements" class="control-label">Question One (<span class="remaining"></span> words remaining)</label>
            <span class="help-block">{{ trans('questions.one') }}</span>
            {!! $errors->first('achievements', '<span class="help-block">:message</span>') !!}
            {!! Form::textarea('achievements', null, [ 'class' => 'form-control word-counter', 'data-max-words' => '300', 'rows' => 15, ]) !!}
        </div>
        <div class="form-group required @if($errors->has('current_engagements')) has-error @endif">
            <label for="current_engagements" class="control-label">Question Two (<span class="remaining"></span> words remaining)</label>
            <span class="help-block">{{ trans('questions.two') }}</span>
            {!! $errors->first('current_engagements', '<span class="help-block">:message</span>') !!}
            {!! Form::textarea('current_engagements', null, [ 'class' => 'form-control word-counter', 'data-max-words' => '300', 'rows' => 15, ]) !!}
        </div>
        <div class="form-group required @if($errors->has('ambitions_plan_of_action')) has-error @endif">
            <label for="ambitions_plan_of_action" class="control-label">Question Three (<span class="remaining"></span> words remaining)</label>
            <span class="help-block">{{ trans('questions.three') }}</span>
            {!! $errors->first('ambitions_plan_of_action', '<span class="help-block">:message</span>') !!}
            {!! Form::textarea('ambitions_plan_of_action', null, [ 'class' => 'form-control word-counter', 'data-max-words' => '300', 'rows' => 15, ]) !!}
        </div>
        <div class="form-group required @if($errors->has('apra_career_advancement')) has-error @endif">
            <label for="apra_career_advancement" class="control-label">Question Four (<span class="remaining"></span> words remaining)</label>
            <span class="help-block">{{ trans('questions.four') }}</span>
            {!! $errors->first('apra_career_advancement', '<span class="help-block">:message</span>') !!}
            {!! Form::textarea('apra_career_advancement', null, [ 'class' => 'form-control word-counter', 'data-max-words' => '300', 'rows' => 15, ]) !!}
        </div>
    </div>
</div>

<script>
    var default_max_words = 300;

    $(function () {
        $('.word-counter').keyup(function () {
            var counter    = $(this).parent().find('label').find('.remaining'),
                max_words  = getMaxWords($(this)),
                word_count = getWordCount($(this).val()),
                remaining  = Math.max(0, max_words - word_count);

            if ( word_count > max_words ) {
                var trimmed = $(this).val().split(/\s+/, max_words).join(' ');
                $(this).val(trimmed + " ");
            }

            if ( typeof counter !== 'undefined' ) counter.text(remaining);
        });

        $('.word-counter').each(function () {
            var counter    = $(this).parent().find('label').find('.remaining'),
                max_words  = getMaxWords($(this)),
                word_count = getWordCount($(this).val()),
                remaining  = Math.max(0, max_words - word_count);

            if ( typeof counter !== 'undefined' ) counter.text(remaining);
        });
    });

    function getWordCount(input) {
        return input.match(/\S+/g) ? input.match(/\S+/g).length : 0;
    }

    function getMaxWords(input) {
        return typeof input.prop('data-max-words') !== 'undefined' ? input.prop('data-max-words') : default_max_words
    }
</script>

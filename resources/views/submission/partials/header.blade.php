<div class="row">
    <div class="col-md-2">
        <img src="{{ $submission->profileImageLink() }}" alt="Performer profile picture" height="180" width="180" />
    </div>
    <div class="col-md-5">
        <h1>{{ $submission->user->name }}</h1>
        <h2>{{ $submission->performer_name or '&nbsp;'}}</h2>

        <input type="hidden" id="active-tab-name" name="active_tab" value="{{ $active_tab }}">

        <ul class="nav nav-tabs submission" role="tablist">
            <li role="presentation" @if($active_tab == 'details') class="active" @endif>
                <a href="#details" aria-controls="home" role="tab" data-toggle="tab" title="{{ $submission->detailsComplete ? 'This section has been completed' : 'This section has incomplete fields' }}">My Details @if(! auth()->user()->hasRole('Staff')) <i class="fa fa-{{ $submission->detailsComplete ? 'check-circle-o' : 'warning' }}"></i> @endif </a>
            </li>
            <li role="presentation" @if($active_tab == 'songs') class="active" @endif>
                <a href="#songs" aria-controls="songs" role="tab" data-toggle="tab" title="{{ $submission->workComplete ? 'This section has been completed' : 'This section has incomplete fields' }}">Work or Songs @if(! auth()->user()->hasRole('Staff')) <i class="fa fa-{{ $submission->workComplete ? 'check-circle-o' : 'warning' }}"></i> @endif </a>
            </li>
            <li role="presentation" @if($active_tab == 'answers') class="active" @endif>
                <a href="#answers" aria-controls="answers" role="tab" data-toggle="tab" title="{{ $submission->answersComplete ? 'This section has been completed' : 'This section has incomplete fields' }}">Answers @if(! auth()->user()->hasRole('Staff')) <i class="fa fa-{{ $submission->answersComplete ? 'check-circle-o' : 'warning' }}"></i> @endif </a>
            </li>
            @if(auth()->user()->hasRole('Staff'))
                <li role="presentation">
                    <a href="#verifications" aria-controls="verifications" role="tab" data-toggle="tab">Verifications</a>
                </li>
            @endif
        </ul>
    </div>
    <div class="col-md-5">
        <div class="row">
            <div class="col-md-5">
                @if( ! isset($review) && ! isset($assessing))
                    <div class="form-group required @if($errors->has('category_id')) has-error @endif">
                        {!! Form::label('category_id', 'Category', [ 'class' => 'control-label', ]) !!}
                        {!! Form::select('category_id', $categories, null, [ 'class' => 'form-control', 'id' => 'category_id', ]) !!}
                        {!! $errors->first('category_id', '<span class="help-block">:message</span>') !!}
                    </div>
                @else
                    <dl>
                        <dt>Category</dt>
                        <dd>{{ $submission->category->name or 'Not set' }}</dd>
                    </dl>
                @endif
            </div>
            <div class="col-md-7">
                @if( ! isset($review) && ! isset($assessing))
                    <div class="form-group required @if($errors->has('partner_id')) has-error @endif">
                        {!! Form::label('partner_id', 'Partner Organisation', [ 'class' => 'control-label', ]) !!}&nbsp;<i class="fa fa-question-circle modal-trigger" data-toggle="modal" data-target="#modal_info-partner"></i>
                        {!! Form::select('partner_id', $partners, null, [ 'class' => 'form-control', 'disabled' => 'disabled', 'id' => 'partner_id', ]) !!}
                        {!! $errors->first('partner_id', '<span class="help-block">:message</span>') !!}
                    </div>
                @else
                    <dl>
                        <dt>Partner Organisation</dt>
                        <dd>{{ $submission->partner->name or 'Not set' }}</dd>
                    </dl>
                @endif
            </div>
        </div>

        <div class="clearfix">&nbsp;</div>

        @if( ! isset($review) && ! isset($assessing))
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="{{ $submission->progress_percent }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $submission->progress_percent }}%;">
                    <span class="sr-only">{{ $submission->progress_percent }}% complete</span>
                    {{ $submission->progress_percent }}% complete
                </div>
            </div>
        @endif

        @if(isset($review))
            @if(auth()->user()->id == $submission->user_id)
                @if($submission->isPending)
                    {!! Form::open([ 'route' => [ 'submission.submit', $submission->hash, ], 'class' => 'pull-right', 'method' => 'post', 'role' => 'form', ]) !!}
                    <div class="btn-group btn-group-md">
                        <a href="{{ route('submission.edit', [ $submission->hash, ]) }}" class="btn btn-md btn-apra" title="Edit your application">Edit Application</a>
                        @if($submission->progress_percent == 100)
                            {!! Form::submit('Submit Application', [ 'class' => 'btn btn-md btn-success', 'id' => 'submit-application', 'onclick' => 'javascript: return confirm("Are you sure you want to submit this assessment?")', ]) !!}
                        @endif
                    </div>
                    {!! Form::close() !!}
                @else
                    <h1 class="pull-right">Application Submitted</h1>
                @endif
            @else
                <h1 class="pull-right">Application Review @if(auth()->user()->hasRole('Staff')) <small>{{ $submission->status->name }}</small> @endif</h1>
                @if(auth()->user()->hasRole('Staff'))
                    <div class="clearfix">&nbsp;</div>
                    @if( ! $submission->verified) <button class="btn btn-md btn-primary pull-right" id="verify-application">Verify Application</button> @endif
                @endif
            @endif
        @elseif(isset($assessing))
            <h1 class="pull-right clearfix">Assessed <span id="assessment-progress"></span></h1>
            <div class="clearfix">&nbsp;</div>
            @if(! $submission->userAssessment)
                <button class="btn btn-md btn-primary pull-right" id="assess-application" onclick="javascript: return confirm('Are you sure you want to submit this assessment?');" type="submit">Submit Assessment</button>
            @endif

            <h2 class="pull-right">
                @if($submission->userAssessment)Application already assessed @else Score @endif
                (<span id="assessment-score">{{ $submission->userAssessment->totalScore or 0 }}</span>/{{ $maximumScore }})&nbsp;
            </h2>
        @else
            <div class="btn-group btn-group-md pull-right">
                {!! Form::submit('Save Application', [ 'class' => 'btn btn-md btn-primary btn-apra', 'id' => 'save-submission', ]) !!}
                {!! Form::submit('Review Application', [ 'class' => 'btn btn-md btn-success', 'id' => 'review-submission', 'name' => 'review_application', ]) !!}
            </div>
        @endif
    </div>
</div>

<div class="modal fade" id="modal_info-partner" tabindex="-1" role="dialog" aria-labelledby="modal-label_info-partner">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" arial-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Partner Organisations</h4>
            </div>
            <div class="modal-body">
                <p>APRA AMCOS teams with a variety of Partner Organisation in order to assist with the application process for the Professional Development Awards.</p>
                <p>These organisations are chosen based on their expertise and involvement with the music industry, and will review each application in order to provide a shortlist to our judges.</p>
                <p>Once you have chosen your category, you can choose from any of the Partner Organisations available in the drop down menu.</p>
            </div>
        </div>
    </div>
</div>

<script>
    $(function() {
        var partners = $('#partner_id');

        @if(isset($partners_disabled) && ! $partners_disabled)
            partners.removeAttr('disabled');
        @endif

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $('#active-tab-name').val($('.nav-tabs.submission .active > a').prop('href').split('#', 2).pop());
        });

        $('#category_id').change(function () {
            var category_id = $(this).val();

            if (! category_id) {
                partners.empty();
                partners.append('<option value="">Select category first</option>');
                partners.attr('disabled', 'disabled');
            } else {
                $.ajax({
                    method: 'GET',
                    dataType: 'json',
                    url: '{{ route('api.category.partners') }}',
                    data: {
                        'category_id': category_id
                    },
                    beforeSend: function () {
                        partners.empty();
                        partners.append('<option value="">Loading partner organisations...</option>');
                        partners.attr('disabled', 'disabled');
                    },
                    success: function (response) {
                        partners.empty();

                        $.each(response, function(key, value) {
                            partners.append('<option value="' + value.id + '">' + value.name + '</option>');
                        });
                    },
                    complete: function () {
                        partners.attr('disabled', false);
                    }
                });
            }
        });
    });
</script>

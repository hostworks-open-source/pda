<tr>
    <td style="white-space: nowrap;">
    	{{ $verification->user->name }}<br />
    	on {{ $verification->created_at->format('d/m/Y') }}
    </td>
    <td>{{ $verification->accepted ? 'Yes' : 'No' }}</td>
    <td>{!! $verification->reason ? $verification->present()->reason : 'N/A' !!}</td>
    <td>@if( $verification->accepted) N/A @else {{ $verification->can_resubmit ? 'Yes' : 'No' }} @endif</td>
</tr>

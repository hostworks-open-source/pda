<div class="alert alert-info">
    <p>You must attach three files with your submission.</p>
    <p>You must click <strong>Save Application</strong> for your files to be uploaded. Saving the application without attaching a file will not save any text in the title and description fields.</p>
    <p>Film &amp; TV and Classical entrants must upload an accompanying score with each work below.</p>
</div>
<div class="row">
    <div class="col-md-4">
        @include('submission.partials.work_upload', [ 'dropzone_id' => 'submission_file_1', 'file' => isset($submission_files->submission_file_1) ? $submission_files->submission_file_1 : new Pda\Entities\SubmissionFile, ])
    </div>
    <div class="col-md-4">
        @include('submission.partials.work_upload', [ 'dropzone_id' => 'submission_file_2', 'file' => isset($submission_files->submission_file_2) ? $submission_files->submission_file_2 : new Pda\Entities\SubmissionFile, ])
    </div>
    <div class="col-md-4">
        @include('submission.partials.work_upload', [ 'dropzone_id' => 'submission_file_3', 'file' => isset($submission_files->submission_file_3) ? $submission_files->submission_file_3 : new Pda\Entities\SubmissionFile, ])
    </div>
</div>

<script>
    var uploads = 0, failures = 0;

    $(function () {
        Dropzone.autoDiscover = false;

        var options = {
            url: '{{ route('submission.fileupload', [ $submission->hash, ]) }}',
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            },
            addRemoveLinks: true,
            maxFiles: 2,
            autoProcessQueue: false,
            init: function() {
                var myDropzone = this;
                var container = $('#' + this.options.containerName);

                $('#save-submission').click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    myDropzone.processQueue();
                });

                this.on('addedfile', function () {
                    uploads++;
                });

                this.on('sending', function (file, xhr, formData) {
                    formData.append('file_group', container.find('.form-control.file_group').val());
                    formData.append('title', container.find('.form-control.title').val());
                    formData.append('description', container.find('.form-control.description').val());
                });

                this.on('removedfile', function (file) {
                    $.ajax({
                        url: '{{ route('submission.deletefile', [ $submission->hash, ]) }}',
                        type: 'delete',
                        data: {
                            "file": file.name
                        },
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        }
                    });

                    uploads--;
                });

                this.on('success', function () {
                    uploads--;
                });
            }
        }

        $('#save-submission').click(function () {
            var waitForUploads = setInterval(function () {
                if (uploads == 0 && failures == 0) {
                    clearInterval(waitForUploads);
                    $('#submission-form').submit();
                }
            }, 1000);
        });

        Dropzone.options.submissionFile1 = options;
        Dropzone.options.submissionFile2 = options;
        Dropzone.options.submissionFile3 = options;

        var dropzone_submission_file_1 = new Dropzone('#submission_file_1', { containerName: 'submission_file_1' });
        var dropzone_submission_file_2 = new Dropzone('#submission_file_2', { containerName: 'submission_file_2' });
        var dropzone_submission_file_3 = new Dropzone('#submission_file_3', { containerName: 'submission_file_3' });

        @foreach($submission_files as $key => $files)            
            @foreach($files as $file)
                var mock = { name: "{!! $file->filename !!}", size: {{ $file->filesize }} };
                dropzone_{{ $key }}.options.addedfile.call(dropzone_{{ $key }},  mock)
            @endforeach
        @endforeach

        $('.form-control.title').keyup(function () {
            var hidden_field = $(this).parent().parent().find('.file-hidden-title');

            if(hidden_field) {
                hidden_field.val($(this).val());
            }
        });

        $('.form-control.description').keyup(function () {
            var hidden_field = $(this).parent().parent().find('.file-hidden-description');

            if(hidden_field) {
                hidden_field.val($(this).val());
            }
        });

        $('#category_id').change(function () {
            getSubmissionFieldRequirements($(this).val());
        });
    });

    function getSubmissionFieldRequirements(category_id)
    {
        $.ajax({
                type: 'GET',
                dataType: 'json',
                url: '{{ route('api.submission.file_requirements') }}',
                data: {
                    "category_id": category_id
                },
                success: function (response) {
                    if (response) {
                        $.each(response, function (key, item) {
                            setSubmissionFileRequired(key, item);
                        });
                    }
                }
            });
    }

    function setSubmissionFileRequired(key, item)
    {
        var container   = $('#' + key),
            title       = container.find('.form-group.title'),
            description = container.find('.form-group.description');

        item.title ? title.addClass('required') : title.removeClass('required');
        item.description ? description.addClass('required') : description.removeClass('required');
    }

    @if($submission->category_id)
    getSubmissionFieldRequirements({{ $submission->category_id}});
    @endif
</script>

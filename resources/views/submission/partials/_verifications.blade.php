<table class="table table-condensed table-striped">
    <thead>
    <tr>
        <th>Verified</th>
        <th>Accepted</th>
        <th>Reason</th>
        <th>Can Resubmit</th>
    </tr>
    </thead>
    <tbody>
    @each('submission.partials.verification_row', $submission->verifications, 'verification', 'raw|<tr><td colspan="4">No verifications at this time</td></tr>')
    </tbody>
</table>

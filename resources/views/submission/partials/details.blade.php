<div class="row">
    <div class="col-md-7">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group required @if($errors->has('name_first')) has-error @endif">
                    {!! Form::label('name_first', 'Name', [ 'class' => 'control-label', ]) !!}
                    {!! Form::text('name_first', $submission->user->name_first, [ 'class' => 'form-control', 'placeholder' => 'First Name', ]) !!}
                    {!! $errors->first('name_first', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group @if($errors->has('name_last')) has-error @endif">
                    {{-- We still need this to pad down the input into the right place, at the same level as first name --}}
                    {!! Form::label('name_last', 'Last', [ 'style' => 'visibility: hidden;', ]) !!}
                    {!! Form::text('name_last', $submission->user->name_last, [ 'class' => 'form-control', 'placeholder' => 'Last Name', ]) !!}
                    {!! $errors->first('name_last', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group required @if($errors->has('performer_name')) has-error @endif">
                    {!! Form::label('performer_name', 'Performer Name or Band/Group', [ 'class' => 'control-label', ]) !!}
                    {!! Form::text('performer_name', null, [ 'class' => 'form-control', 'placeholder' => 'Performing Name', ]) !!}
                    <span class="help-block">Will be used online or in print if a finalist.</span>
                    {!! $errors->first('performer_name', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group required @if($errors->has('address_1')) has-error @endif">
                    {!! Form::label('address_1', 'Address Line 1', [ 'class' => 'control-label', ]) !!}
                    {!! Form::text('address_1', null, [ 'class' => 'form-control', 'placeholder' => 'Address Line 1', ]) !!}
                    {!! $errors->first('address_1', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group @if($errors->has('address_2')) has-error @endif">
                    {!! Form::label('address_2', 'Address Line 2', [ 'class' => 'control-label', ]) !!}
                    {!! Form::text('address_2', null, [ 'class' => 'form-control', 'placeholder' => 'Address Line 2', ]) !!}
                    {!! $errors->first('address_2', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group required @if($errors->has('city')) has-error @endif">
                    {!! Form::label('city', 'City', [ 'class' => 'control-label', ]) !!}
                    {!! Form::text('city', null, [ 'class' => 'form-control', 'placeholder' => 'City', ]) !!}
                    {!! $errors->first('city', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group required @if($errors->has('state')) has-error @endif">
                    {!! Form::label('state', 'State', [ 'class' => 'control-label', ]) !!}
                    {!! Form::text('state', null, [ 'class' => 'form-control', 'placeholder' => 'State', ]) !!}
                    {!! $errors->first('state', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group required @if($errors->has('postcode')) has-error @endif">
                    {!! Form::label('postcode', 'Postcode', [ 'class' => 'control-label', ]) !!}
                    {!! Form::text('postcode', null, [ 'class' => 'form-control', 'placeholder' => 'Postcode', ]) !!}
                    {!! $errors->first('postcode', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group required @if($errors->has('country')) has-error @endif">
                    {!! Form::label('country', 'Country', [ 'class' => 'control-label', ]) !!}
                    {!! Form::text('country', null, [ 'class' => 'form-control', 'placeholder' => 'Country', ]) !!}
                    {!! $errors->first('country', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group required @if($errors->has('phone_number')) has-error @endif">
                    {!! Form::label('phone_number', 'Phone Number', [ 'class' => 'control-label', ]) !!}
                    {!! Form::text('phone_number', null, [ 'class' => 'form-control', 'placeholder' => 'Phone Number', ]) !!}
                    {!! $errors->first('phone_number', '<span class="help-block">:message</span>') !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group required @if($errors->has('profile_image')) has-error @endif">
                    {!! Form::label('profile_image', 'Profile Picture', [ 'class' => 'control-label', ]) !!}
                    {!! Form::file('profile_image', null, [ 'class' => 'form-control', ]) !!}
                    {!! $errors->first('profile_image', '<span class="help-block">:message</span>') !!}
                    @if($submission->profile_image)
                        <span class="help-block"><strong>You have already attached a profile image. If you specify a new image, it will overwrite the existing one.</strong></span>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>


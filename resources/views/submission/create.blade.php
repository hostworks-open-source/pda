@extends('layouts.master')

@section('content')
    @include('flash::message')
    
    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Create an application</div>
                </div>
                <div class="panel-body">
                    {!! Form::open([ 'route' => 'submission.store', 'class' => 'form-horizontal', 'method' => 'post', 'role' => 'form', ]) !!}
                        <div class="form-group @if($errors->has('first_name')) has-error @endif">
                            {!! Form::text('name_first', null, [ 'class' => 'form-control', 'placeholder' => 'First Name', ]) !!}
                            {!! $errors->first('name_first', '<span class="help-block">:message</span>') !!}
                        </div>

                        <div class="form-group @if($errors->has('last_name')) has-error @endif">
                            {!! Form::text('name_last', null, [ 'class' => 'form-control', 'placeholder' => 'Last Name', ]) !!}
                            {!! $errors->first('name_last', '<span class="help-block">:message</span>') !!}
                        </div>

                        <div class="clearfix">&nbsp;</div>

                        <div class="form-group @if($errors->has('email')) has-error @endif">
                            {!! Form::email('email', null, [ 'class' => 'form-control', 'placeholder' => 'Email Address', ]) !!}
                            <p class="help-block">We need this to activate your account, and in the event you need to recover your password.</p>
                            {!! $errors->first('email', '<span class="help-block">:message</span>') !!}
                        </div>

                        <div class="form-group @if($errors->has('password')) has-error @endif">
                            {!! Form::password('password', [ 'class' => 'form-control', 'placeholder' => 'Password', ]) !!}
                            {!! $errors->first('password', '<span class="help-block">:message</span>') !!}
                        </div>

                        <button class="btn btn-lg btn-block btn-primary">Start my application</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

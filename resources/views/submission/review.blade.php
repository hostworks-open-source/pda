@extends('layouts.master')

@section('footer_scripts')
    
    <!-- insert JW Player script here -->
    
    <script>jwplayer.key="{{ env('JWPLAYER_KEY') }}";</script>
    @if(isset($assessing))
        <script>
        $(function() {
            $('[data-rangeslider]').rangeslider({polyfill: false});

            $(document).on('input', '[data-rangeslider]', function (el) {
                setOutputValue(el.target);
                updateAssessables();
            });

            for (var i = $(['data-rangeslider']).length - 1; i >= 0; i--) {
                setOutputValue($('[data-rangeslider]')[i]);
            }

            function setOutputValue(el) {
                el.parentNode.getElementsByTagName('output')[0].innerHTML = el.value;
            }

            if ($('#assessment-progress').length) {
                updateAssessables();
            }
        });

        function updateAssessables()
        {
            var assessables = $('[data-rangeslider]'),
                assessed    = $('[data-rangeslider]').filter(function () { return $(this).val() != 0; }),
                totalScore  = 0;

            $.each(assessables, function () {
                totalScore += parseInt($(this).val());
            });

            $('#assessment-score').text(totalScore);

            $('#assessment-progress').text(assessed.length + '/' + assessables.length + ' items');
        }
        </script>
    @endif
@stop

@section('content')
    @if(isset($assessing) && ! $submission->userAssessment)
    {!! Form::open([ 'route' => [ 'submission.assess', $submission->hash, ], 'method' => 'post', 'role' => 'form', ]) !!}
    {!! Form::hidden('assessment_group_id', $assessment_group_id) !!}
    @endif

    <div id="submission-form">
        @include('submission.partials.header')

        <div class="clearfix">&nbsp;</div>

        @include('flash::message')

        <div class="row">
            <div class="col-md-12">
                <div class="tab-content">
                    <div class="clearfix">&nbsp;</div>

                    <div role="tabpanel" class="tab-pane fade in active" id="details">
                        <d1>
                            <dt>Name</dt>
                            <dd>{{ $submission->user->name }}</dd>
                            <dt>Performing Name</dt>
                            <dd>{{ $submission->performer_name or 'Not set' }}</dd>
                            @if(auth()->user()->hasRole('Staff', 'Admin') || auth()->id() == $submission->user_id)
                                <dt>Address 1</dt>
                                <dd>{{ $submission->address_1 }}</dd>
                                <dt>Address 2</dt>
                                <dd>{{ $submission->address_2 or 'Not set' }}</dd>
                                <dt>City</dt>
                                <dd>{{ $submission->city }}</dd>
                                <dt>State</dt>
                                <dd>{{ $submission->state }}</dd>
                                <dt>Postcode</dt>
                                <dd>{{ $submission->postcode }}</dd>
                                <dt>Country</dt>
                                <dd>{{ $submission->country }}</dd>
                                <dt>Phone Number</dt>
                                <dd>{{ $submission->phone_number }}</dd>
                            @endif
                        </d1>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="songs">
                        @if(isset($assessing))
                            @include('partials.input_range', [ 'name' => 'submission_files', 'max' => 20, 'title' => 'Please rank all 3 works out of a combined total of 20', 'value' => $submission->userAssessment ? $submission->userAssessment->submission_files : 0, ])
                        @endif

                        @unless($submission_files->isEmpty())
                            <ul class="nav nav-tabs" role="tablist">
                                @foreach($submission_files as $key => $file)
                                    <li role="presentation" @if($key == 'submission_file_1') class="active" @endif>
                                        <a href="#media_{{ $key }}" aria-controls="media_{{ $key }}" role="tab" data-toggle="tab">{{ $file[0]->title }}</a>
                                    </li>
                                @endforeach
                            </ul>

                            <div class="tab-content">
                                <div class="clearfix">&nbsp;</div>

                                @foreach($submission_files as $key => $files)
                                    <div class="tab-pane @if($key == 'submission_file_1') active @endif" id="media_{{ $key }}" role="tabpanel">
                                        <h2>{{ $files[0]->title }}</h2>
                                        {!! $files[0]->present()->description !!}

                                        @foreach($files as $file)
                                            <div class="text-center media-preview">
                                                @if(starts_with($file->mime_type, 'audio/'))
                                                    <audio controls id="audio_stream_{{ $file->id }}" height="40" preload="metadata" width="400">
                                                        <source src="{{ route('submission.media', [ $submission->hash, $file->filename, ]) }}" type="{{ $file->mime_type }}">
                                                        <p>Your browser does not support the audio element.</p>
                                                    </audio>
                                                @elseif(starts_with($file->mime_type, 'video/'))
                                                    <video id="video_stream_{{ $file->id }}" preload="metadata">
                                                        <source source src="{{ route('submission.media', [ $submission->hash, $file->filename, ]) }}" type="{{ $file->mime_type }}">
                                                        <p>Your browser does not support the video element.</p>
                                                    </video>
                                                    <script>
                                                        $(function() {
                                                            jwplayer('video_stream_{{ $file->id }}').setup({
                                                                file: "{{ route('submission.media', [ $submission->hash, $file->filename, ]) }}",
                                                                androidhls: true,
                                                                width: '100%',
                                                                aspectratio: '16:9'
                                                            });
                                                        });
                                                    </script>
                                                @elseif($file->isImage())
                                                    <img src="{{ route('submission.media', [ $submission->hash, $file->filename, ]) }}" class="img-responsive media" />
                                                @else
                                                    Download non-playable media {!! link_to_route('submission.media', $file->filename, [ $submission->hash, $file->filename, ]) !!}.
                                                @endif

                                                @if(starts_with($file->mime_type, [ 'audio/', 'video/', ]))
                                                    <p>If the above player does not work, you can download the file for playback on your computer {!! link_to_route('submission.media', 'here', [ $submission->hash, $file->filename, ], [ 'download', 'target' => '_blank',]) !!}.</p>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                @endforeach
                            </div>
                        @endunless
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="answers">
                        <dl>
                            <dt>Proof of identity</dt>
                            <dd>@if($submission->proof_of_identity)<img src="{{ $submission->proofOfIdentityLink() }}" alt="Performer proof of identity" width="600" /> @else Not set @endif</dd>
                            <dt>Biography</dt>
                            <dd>{!! $submission->present()->biography !!}</dd>
                            <dt>Question One &mdash; {{ trans('questions.one') }}</dt>
                            <dd>{!! $submission->present()->achievements !!}</dd>
                            @if(isset($assessing))
                                @include('partials.input_range', [ 'name' => 'achievements', 'max' => 20, 'value' => $submission->userAssessment ? $submission->userAssessment->achievements : 0, ])
                            @endif
                            <dt>Question Two &mdash; {{ trans('questions.two') }}</dt>
                            <dd>{!! $submission->present()->currentEngagements !!}</dd>
                            @if(isset($assessing))
                                @include('partials.input_range', [ 'name' => 'current_engagements', 'max' => 20, 'value' => $submission->userAssessment ? $submission->userAssessment->current_engagements : 0, ])
                            @endif
                            <dt>Question Three &mdash; {{ trans('questions.three') }}</dt>
                            <dd>{!! $submission->present()->ambitionsPlanOfAction !!}</dd>
                            @if(isset($assessing))
                                @include('partials.input_range', [ 'name' => 'ambitions_plan_of_action', 'max' => 20, 'value' => $submission->userAssessment ? $submission->userAssessment->ambitions_plan_of_action : 0, ])
                            @endif
                            <dt>Question Four &mdash; {{ trans('questions.four') }}</dt>
                            <dd>{!! $submission->present()->apraCareerAdvancement !!}</dd>
                            @if(isset($assessing))
                                @include('partials.input_range', [ 'name' => 'apra_career_advancement', 'max' => 20, 'value' => $submission->userAssessment ? $submission->userAssessment->apra_career_advancement : 0, ])
                            @endif
                        </dl>
                    </div>
                    @if(auth()->user()->hasRole('Staff'))
                        <div role="tabpanel" class="tab-pane fade" id="verifications">
                            @include('submission.partials._verifications')
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @if(isset($assessing) && ! $submission->userAssessment) {!! Form::close() !!} @endif

    @if(auth()->user()->hasRole('Staff') && ! $submission->verified)
        @include('submission.verify_modal')

        <script>
        $(function() {
            $('#verify-application').click(function () {
                $('#modal-verify-application').modal();
            });

            $('input[name="accepted"]').click(function () {
                $(this).val() == 0 ? $('#reject-reason').removeClass('hidden') : $('#reject-reason').addClass('hidden');
            });

            $('#submit-verification').click(function () {
                $('#form-verify-application').submit();
            });

            @if($errors->has('accepted')) $('#modal-verify-application').modal(); @endif

            $('#reject_reason_template').change(function () {
                var reason_field = $('#reject_reason'),
                    reject_template = $(this).val();

                if (reject_template == 'not_individual') {
                    reason_field.text('We note that you have applied on behalf of your band/group – for the purposes of the PDA program we ask that all applicants apply as individuals. Your supplied tracks and listed achievements can reflect those of your band/group, however your application needs to be from one individual.')
                } else if (reject_template == 'corrupt_file') {
                    reason_field.text('Unfortunately the files you have uploaded on our system appear to not be working – the files may be corrupt or possibly have been received incomplete.');
                } else if ( reject_template == 'no_video') {
                    reason_field.text('You have entered under the Film & TV award which requires applicants to provide audiovisual samples of their screen composing work. Do you have three such examples to provide or should I change your category to Popular Contemporary?');
                } else {
                    reason_field.text('');
                }
            });
        });
        </script>
    @endif
@stop

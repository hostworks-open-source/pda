<div class="input_range_container">
	<h3>@if(isset($title)) {{ $title }} @else Score out of {{ $max or 10 }} @endif</h3>
	<input name="{{ $name }}" id="range-{{ $name }}" class="input_range" max="{{ $max or 10 }}" min="{{ $min or 0 }}" step="{{ $step or 1 }}" type="range" value="{{ $value or 0 }}" data-rangeslider>
	<output>{{ $value or 0 }}</output>
</div>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-COMPATIBLE" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ env('SITE_NAME') }}</title>

    <link href="{!! elixir('css/app.css') !!}" rel="stylesheet" type="text/css">
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
    @yield('header_styles')

    <script src="{!! elixir('js/app.js') !!}"></script>
    <!--[if lt IE 9]>
    <script src="{!! elixir('js/bc.js') !!}"></script>
    <![endif]-->
</head>
<body>
@include('partials.navigation')

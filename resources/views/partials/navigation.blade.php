<nav class="navbar navbar-inverse">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li>{!! link_to_route('home', 'Home') !!}</li>
            @if( ! auth()->guest())
                <li><a href="{{ route('my.submission') }}" title="View my submission">{{ auth()->user()->name }}</a></li>
                @if(auth()->user()->hasRole('Staff')) <li>{!! link_to_route('admin.applications.index', 'Staff') !!}</li> @endif
                @if(auth()->user()->hasRole('Partner')) <li>{!! link_to_route('admin.applications.accepted.index', 'Partners') !!}</li> @endif
                @if(auth()->user()->hasRole('Judge')) <li>{!! link_to_route('admin.applications.shortlisted.index', 'Judges') !!}</li> @endif
                <li>{!! link_to('auth/logout', 'Logout') !!}</li>
            @else
                <li><a href="{{ url('auth/login') }}">Login</a></li>
            @endif
        </ul>
    </div><!-- /.container-fluid -->
</nav>

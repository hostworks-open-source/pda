	<div class="footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					&copy; {{ date('Y') }} APRA AMCOS
				</div>
				<div class="col-md-5 col-md-offset-3 text-right contact-details">
					<a href="{{ env('URL_LIVE_CHAT') }}" target="_blank" title="Live Chat">Live Chat</a><span class="text-muted">/</span>
					{!! link_to('mailto:' . env('ORG_CONTACT_EMAIL'), env('ORG_CONTACT_EMAIL'), [ 'title' => 'Email Us', ]) !!}<span class="text-muted">/</span>
					{{ env('ORG_CONTACT_PHONE') }}
				</div>
			</div>
		</div>
	</div>

	@yield('footer_scripts')
</body>
</html>

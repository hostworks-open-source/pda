<p>Hi {{ $submission->user->name_first }},</p>

<p>This email is to let you know that APRA has verified your application for the 2015 Professional Development Awards.</p>

@if($accepted)
    <p>We are pleased to inform you that your application has been accepted and has now been passed along to your chosen Partner Organisation for review.</p>
@else
    <p>We regret to inform you that your application has been rejected.</p>

    @if($reason)
        <p>{{ $reason }}</p>
    @endif

    @if($resubmit)
        <p>Once you have addressed the feedback above, you may resubmit your application for verification.</p>
    @endif
@endif

@include('emails.application.signature')

<p>Kind Regards,</p>

<p>APRA AMCOS</p>
<p>{!! link_to('mailto:' . env('ORG_CONTACT_EMAIL'), env('ORG_CONTACT_EMAIL'), [ 'title' => 'Email Us', ]) !!}</p>
<p>{{ env('ORG_CONTACT_PHONE') }}</p>
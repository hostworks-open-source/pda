<p>Hi {{ $submission->user->name_first }},</p>

<p>This email is to confirm that we have received your application for the 2015 APRA Professional Development Awards.</p>

<p>Thank you for your submission, and we wish you well through the application process.</p>

@include('emails.application.signature')

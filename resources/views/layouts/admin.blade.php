@include('partials.header')

<div class="admin-header">
	@include('admin.partials.navigation')

	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2>@yield('page_heading')</h2>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            @include('flash::message')
            @yield('content')
        </div>
    </div>
</div>

@include('partials.footer')

<?php

use Illuminate\Database\Seeder;

class PartnersTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('partners')->truncate();
        DB::statement('ALTER TABLE `submissions` AUTO_INCREMENT=1');
        DB::table('partners')->insert([
            [ 'id' => 1, 'name' => 'AAM - Association of Artist Managers', 'max_applicants' => 180, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 2, 'name' => 'Association of Independent Record Labels', 'max_applicants' => 150, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 3, 'name' => 'Australian Film, Television and Radio School', 'max_applicants' => 60, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 4, 'name' => 'Australian Guild of Screen Composers', 'max_applicants' => 50, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 5, 'name' => 'Australian Institute of Music', 'max_applicants' => 100, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 6, 'name' => 'Australian Music Centre', 'max_applicants' => 200, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 7, 'name' => 'Carclew Youth Arts Centre', 'max_applicants' => 20, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 8, 'name' => 'Country Music Association of Australia', 'max_applicants' => 60, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 9, 'name' => 'Country Music SA', 'max_applicants' => 30, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 10, 'name' => 'Gadigal Information Service (Aboriginal Corporation)', 'max_applicants' => 20, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 11, 'name' => 'Jazzgroove Association', 'max_applicants' => 50, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 12, 'name' => 'Melbourne Jazz Co-Operative', 'max_applicants' => 30, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 13, 'name' => 'Music NT', 'max_applicants' => 40, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 14, 'name' => 'Music SA', 'max_applicants' => 85, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 15, 'name' => 'Music Victoria', 'max_applicants' => 230, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 16, 'name' => 'Queensland Music Network Inc', 'max_applicants' => 100, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 17, 'name' => 'Songlines', 'max_applicants' => 15, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 18, 'name' => 'Talent Development Project', 'max_applicants' => 80, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 19, 'name' => 'The Push Inc', 'max_applicants' => 50, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 20, 'name' => 'MusicNSW', 'max_applicants' => 220, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 21, 'name' => 'Multicultural Arts VIC', 'max_applicants' => 40, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 22, 'name' => 'Music Tasmania', 'max_applicants' => 25, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
        ]);
    }

}

<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('statuses')->truncate();
        DB::statement('ALTER TABLE `submissions` AUTO_INCREMENT=1');
        DB::table('statuses')->insert([
            [ 'id' => 1, 'name' => 'Pending', 'type' => 'submission', 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 2, 'name' => 'Submitted', 'type' => 'submission', 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 3, 'name' => 'Approved', 'type' => 'submission', 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 4, 'name' => 'Rejected', 'type' => 'submission', 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
        ]);
    }


}

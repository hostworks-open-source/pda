<?php

use Illuminate\Database\Seeder;

class UserTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->truncate();
        DB::statement('ALTER TABLE `submissions` AUTO_INCREMENT=1');
        DB::table('roles')->insert([
            [ 'id' => 1, 'name' => 'Admin', ],
            [ 'id' => 2, 'name' => 'Partner', ],
            [ 'id' => 3, 'name' => 'Staff', ],
            [ 'id' => 4, 'name' => 'Judge', ],
            [ 'id' => 5, 'name' => 'Applicant', ],
        ]);
    }
}

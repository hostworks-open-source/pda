<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::statement('ALTER TABLE `submissions` AUTO_INCREMENT=1');
        DB::table('users')->insert([
            [
                'id'         => 1,
                'name_first' => 'Hostworks',
                'name_last'  => 'Support',
                'email'      => 'support@hostworks.com.au',
                'password'   => bcrypt('*********'),
                'superuser'  => true,
                'role_id'    => 1,
                'created_at' => new \DateTime,
                'updated_at' => new \DateTime,
            ]
        ]);
    }
}

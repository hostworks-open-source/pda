<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        $this->call('UserTypesSeeder');
        $this->call('UserTableSeeder');
        $this->call('CategoriesTableSeeder');
        $this->call('PartnersTableSeeder');
        $this->call('StatusesTableSeeder');
        $this->call('CategoryPartnerTableSeeder');

        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}

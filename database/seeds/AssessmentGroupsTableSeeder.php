<?php

use Illuminate\Database\Seeder;

class AssessmentGroupsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('assessment_groups')->delete();
        DB::statement('ALTER TABLE `assessment_groups` AUTO_INCREMENT = 1');
        DB::table('assessment_groups')->insert([
            [ 'id' => 1, 'name' => 'Partner', ],
            [ 'id' => 2, 'name' => 'Round 1', ],
            [ 'id' => 3, 'name' => 'Round 2', ],
        ]);
    }
}

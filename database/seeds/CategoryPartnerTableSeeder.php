<?php

use Illuminate\Database\Seeder;

class CategoryPartnerTableSeeder extends Seeder
{
	public function run()
	{
		DB::table('category_partner')->truncate();
		DB::table('category_partner')->insert([
			// Australian Film, Television and Radio School
			[ 'category_id' => 5, 'partner_id' => 3, ],
			// Australian Guild of Screen Composers
			[ 'category_id' => 5, 'partner_id' => 4, ],
			// Australian Music Centre
			[ 'category_id' => 2, 'partner_id' => 6, ],
			[ 'category_id' => 3, 'partner_id' => 6, ],
			// Country Music Association of Australia
			[ 'category_id' => 6, 'partner_id' => 8, ],
			// Association of Independent Record Labels
			[ 'category_id' => 1, 'partner_id' => 2, ],
			[ 'category_id' => 6, 'partner_id' => 2, ],
			// AAM - Association of Artist Managers
			[ 'category_id' => 1, 'partner_id' => 1, ],
			[ 'category_id' => 4, 'partner_id' => 1, ],
			[ 'category_id' => 6, 'partner_id' => 1, ],
			// Carclew Youth Arts Centre
			[ 'category_id' => 1, 'partner_id' => 7, ],
			[ 'category_id' => 6, 'partner_id' => 7, ],
			// Music NT
			[ 'category_id' => 1, 'partner_id' => 13, ],
			[ 'category_id' => 3, 'partner_id' => 13, ],
			[ 'category_id' => 4, 'partner_id' => 13, ],
			[ 'category_id' => 6, 'partner_id' => 13, ],
			// Music SA
			[ 'category_id' => 1, 'partner_id' => 14, ],
			[ 'category_id' => 3, 'partner_id' => 14, ],
			[ 'category_id' => 6, 'partner_id' => 14, ],
			// Gadigal Information Service (Aboriginal Corporation)
			[ 'category_id' => 4, 'partner_id' => 10, ],
			// MusicNSW
			[ 'category_id' => 1, 'partner_id' => 20, ],
			[ 'category_id' => 3, 'partner_id' => 20, ],
			[ 'category_id' => 4, 'partner_id' => 20, ],
			[ 'category_id' => 6, 'partner_id' => 20, ],
			// Talent Development Project
			[ 'category_id' => 1, 'partner_id' => 18, ],
			[ 'category_id' => 3, 'partner_id' => 18, ],
			[ 'category_id' => 6, 'partner_id' => 18, ],
			// Queensland Music Network Inc
			[ 'category_id' => 1, 'partner_id' => 16, ],
			[ 'category_id' => 6, 'partner_id' => 16, ],
			// The Push Inc
			[ 'category_id' => 1, 'partner_id' => 19, ],
			[ 'category_id' => 6, 'partner_id' => 19, ],
			// Multicultural Arts VIC
			[ 'category_id' => 1, 'partner_id' => 21, ],
			[ 'category_id' => 3, 'partner_id' => 21, ],
			[ 'category_id' => 4, 'partner_id' => 21, ],
			[ 'category_id' => 6, 'partner_id' => 21, ],
			// Music Victoria
			[ 'category_id' => 1, 'partner_id' => 15, ],
			[ 'category_id' => 3, 'partner_id' => 15, ],
			[ 'category_id' => 4, 'partner_id' => 15, ],
			[ 'category_id' => 6, 'partner_id' => 15, ],
			// Songlines
			[ 'category_id' => 4, 'partner_id' => 17, ],
			// Australian Institute of Music
			[ 'category_id' => 1, 'partner_id' => 5, ],
			[ 'category_id' => 6, 'partner_id' => 5, ],
			// Jazzgroove Association
			[ 'category_id' => 3, 'partner_id' => 11, ],
			// Melbourne Jazz Co-Operative
			[ 'category_id' => 3, 'partner_id' => 12, ],
			// Music Tasmania
			[ 'category_id' => 1, 'partner_id' => 22, ],
			[ 'category_id' => 4, 'partner_id' => 22, ],
			[ 'category_id' => 6, 'partner_id' => 22, ],
			// Country Music SA
			[ 'category_id' => 6, 'partner_id' => 9, ],
		]);
	}
}
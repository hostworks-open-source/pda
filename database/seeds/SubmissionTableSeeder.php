<?php

use Illuminate\Database\Seeder;

class SubmissionTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('submissions')->truncate();
        DB::statement('ALTER TABLE `submissions` AUTO_INCREMENT=1');
        DB::table('submission_files')->truncate();
        DB::statement('ALTER TABLE `submission_files` AUTO_INCREMENT=1');
    }
}

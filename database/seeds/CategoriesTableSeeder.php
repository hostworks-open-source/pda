<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder {

    public function run()
    {
        DB::table('categories')->truncate();
        DB::statement('ALTER TABLE `submissions` AUTO_INCREMENT=1');
        DB::table('categories')->insert([
            [ 'id' => 1, 'name' => 'Popular Contemporary', 'max_shortlist' => 100, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 2, 'name' => 'Classical', 'max_shortlist' => 20, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 3, 'name' => 'Jazz', 'max_shortlist' => 20, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 4, 'name' => 'Aboriginal & Torres Strait Islander (ATSI)', 'max_shortlist' => 10, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 5, 'name' => 'Film & Television', 'max_shortlist' => 20, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 6, 'name' => 'Country', 'max_shortlist' => 30, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
        ]);
    }

}

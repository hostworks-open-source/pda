<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubmissionFileGroup extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('submission_files', function(Blueprint $table)
		{
			$table->string('file_group')->nullable()->default(null)->after('mime_type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('submission_files', function(Blueprint $table)
		{
			$table->dropColumn('file_group');
		});
	}

}

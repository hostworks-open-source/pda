<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryPartnerPivotTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('category_partner', function(Blueprint $table)
		{
			$table->integer('category_id')->unsigned();
			$table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');
			$table->integer('partner_id')->unsigned();
			$table->foreign('partner_id')->references('id')->on('partners')->onUpdate('cascade')->onDelete('cascade');
			$table->unique([ 'category_id', 'partner_id', ]);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('category_partner');
	}

}

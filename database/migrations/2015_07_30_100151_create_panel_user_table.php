<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePanelUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('panel_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('panel_id')->unsigned()->nullable()->default(null);
            $table->foreign('panel_id')->references('id')->on('panels')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->nullable()->default(null);
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('panel_user');
    }
}

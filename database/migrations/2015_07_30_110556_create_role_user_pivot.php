<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Pda\Entities\User;

class CreateRoleUserPivot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_user', function (Blueprint $table) {
            $table->integer('role_id')->unsigned()->nullable()->default(null);
            $table->foreign('role_id')->references('id')->on('roles')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->nullable()->default(null);
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });

        User::all()->each(function ($user) {
            $user->roles()->attach($user->role_id);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_role_id_foreign');
            $table->dropColumn('role_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $roles = DB::table('role_user')->select('role_id', 'user_id')->groupBy('user_id')->get();

        Schema::table('users', function (Blueprint $table) {
            $table->integer('role_id')->unsigned()->nullable()->default(null)->after('superuser');
            $table->foreign('role_id')->references('id')->on('roles')->onUpdate('cascade')->onDelete('cascade');
        });

        foreach ($roles as $role) {
            $user = User::find($role->user_id);
            $user->update([ 'role_id' => $role->role_id, ]);
        }

        Schema::drop('role_user');
    }
}

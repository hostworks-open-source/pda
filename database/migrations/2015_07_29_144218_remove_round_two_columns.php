<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveRoundTwoColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('submissions', function (Blueprint $table) {
            $table->dropColumn([ 'round_two_rank', 'round_two_shortlist', ]);
        });

        DB::table('assessment_groups')->where('id', 3)->delete();
        DB::statement('ALTER TABLE `assessment_groups` AUTO_INCREMENT=2');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('submissions', function (Blueprint $table) {
            $table->tinyInteger('round_two_rank')->unsigned()->default(0)->after('round_one_rank');
            $table->boolean('round_two_shortlist')->default(false)->after('round_one_shortlist');
        });
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Collection;
use Pda\Entities\Role;
use Pda\Entities\User;

class CreateJudgingUsers extends Migration
{
    private $judges;


    public function __construct()
    {
        $this->judges = new Collection([
            [ 'name_first' => 'John', 'name_last' => 'Smith', 'email' => 'johnsmith@email.com', 'password' => 'pass', 'panel_id' => 1, ]
        ]);
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->judges->each(function ($judge) {
            $user = User::where('email', $judge['email'])->first();

            if (! $user) {
                $user = User::createJudge([
                    'name_first' => $judge['name_first'],
                    'name_last'  => $judge['name_last'],
                    'email'      => $judge['email'],
                    'password'   => $judge['password'],
                ]);
            } else {
                // If the user already exists, make sure we sync the existing roles to avoid duplication
                $user->roles()->sync(array_unique(array_merge($user->roles->lists('id'), [ Role::JUDGE, ])));
            }

            $user->panels()->attach($judge['panel_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $users = DB::table('panel_user')->lists('user_id');
        DB::table('users')->whereIn('id', $users)->delete();
        DB::table('panel_user')->truncate();
        DB::statement('ALTER TABLE panel_user AUTO_INCREMENT=1');
    }
}

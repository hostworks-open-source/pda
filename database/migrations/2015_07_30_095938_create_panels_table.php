<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Pda\Entities\AssessmentGroup;
use Pda\Entities\Category;

class CreatePanelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('panels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned()->nullable()->default(null);
            $table->foreign('category_id')->references('id')->on('categories')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('assessment_group_id')->unsigned()->nullable()->default(null);
            $table->foreign('assessment_group_id')->references('id')->on('assessment_groups')->onUpdate('cascade')->onDelete('cascade');
            $table->string('name');
            $table->integer('max_applicants')->unsigned();
            $table->timestamps();
        });

        DB::table('panels')->insert([
            [ 'id' => 1, 'category_id' => Category::JAZZ, 'assessment_group_id' => AssessmentGroup::ROUND_ONE, 'name' => 'Panel 1', 'max_applicants' => 20, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 2, 'category_id' => Category::COUNTRY, 'assessment_group_id' => AssessmentGroup::ROUND_ONE, 'name' => 'Panel 1', 'max_applicants' => 30, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 3, 'category_id' => Category::POPULAR_CONTEMPORARY, 'assessment_group_id' => AssessmentGroup::ROUND_ONE, 'name' => 'Panel 1', 'max_applicants' => 25, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 4, 'category_id' => Category::POPULAR_CONTEMPORARY, 'assessment_group_id' => AssessmentGroup::ROUND_ONE, 'name' => 'Panel 2', 'max_applicants' => 25, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 5, 'category_id' => Category::POPULAR_CONTEMPORARY, 'assessment_group_id' => AssessmentGroup::ROUND_ONE, 'name' => 'Panel 3', 'max_applicants' => 25, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 6, 'category_id' => Category::POPULAR_CONTEMPORARY, 'assessment_group_id' => AssessmentGroup::ROUND_ONE, 'name' => 'Panel 4', 'max_applicants' => 25, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 7, 'category_id' => Category::CLASSICAL, 'assessment_group_id' => AssessmentGroup::ROUND_ONE, 'name' => 'Panel 1', 'max_applicants' => 20, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 8, 'category_id' => Category::FILM_AND_TELEVISION, 'assessment_group_id' => AssessmentGroup::ROUND_ONE, 'name' => 'Panel 1', 'max_applicants' => 20, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
            [ 'id' => 9, 'category_id' => Category::ATSI, 'assessment_group_id' => AssessmentGroup::ROUND_ONE, 'name' => 'Panel 1', 'max_applicants' => 10, 'created_at' => new \DateTime, 'updated_at' => new \DateTime, ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('panels');
    }
}

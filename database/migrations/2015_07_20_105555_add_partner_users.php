<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Pda\Entities\User;

class AddPartnerUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        User::createPartner([ 'name_first' => 'Partner Name', 'name_last' => 'Managers', 'email' => 'email@partner.com', 'password' => 'pass', ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        User::where('role_id', User::USER_TYPE_PARTNER)->whereIn('email', [
            'email@partner.com',
            
        ])->delete();
    }
}

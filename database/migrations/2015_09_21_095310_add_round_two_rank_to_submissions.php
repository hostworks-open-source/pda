<?php

use Pda\Entities\AssessmentGroup;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoundTwoRankToSubmissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('submissions', function (Blueprint $table) {
			$table->tinyInteger('round_two_rank')->unsigned()->default(0)->after('round_one_shortlist');
			$table->dropColumn('round_one_shortlist');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('submissions', function (Blueprint $table) {
			$table->dropColumn('round_two_rank');
			$table->boolean('round_one_shortlist')->default(false)->after('round_one_rank');
		});
	}

}

<?php

use Pda\Entities\Role;
use Pda\Entities\User;
use Pda\Entities\Panel;
use Pda\Entities\Category;
use Pda\Entities\AssessmentGroup;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSecondRoundJudging extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Create the round two assessment group
		$group  = AssessmentGroup::create([ 'name' => 'Round 2', ]);

		// Get the existing round one judges in the popular contemporary category
		$judges = User::ofType(Role::JUDGE)->whereHas('panels', function ($query) {
			$query->where('assessment_group_id', AssessmentGroup::ROUND_ONE)
				  ->where('category_id', Category::POPULAR_CONTEMPORARY);
		})->get();

		// Create a new judging panel for round two popular contemporary
		$panel = Panel::create([ 'name' => 'Panel 1', 'max_applicants' => 20, ]);
		$panel->category()->associate(Category::find(Category::POPULAR_CONTEMPORARY));
		$panel->assessmentGroup()->associate($group);
		$panel->save();

		// Assign those judges to the round two panel
		$judges->each(function ($judge) use ($panel) {
			$judge->panels()->attach($panel);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		$group = AssessmentGroup::where('name', 'Round 2')->first();
		$group->delete();
	}

}

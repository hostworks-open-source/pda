<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssessmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('submission_id')->unsigned()->nullable()->default(null);
            $table->foreign('submission_id')->references('id')->on('submissions')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->nullable()->default(null);
            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->integer('assessment_group_id')->unsigned()->nullable()->default(null);
            $table->foreign('assessment_group_id')->references('id')->on('assessment_groups')->onUpdate('cascade')->onDelete('cascade');
            $table->tinyInteger('submission_files')->unsigned()->default(0);
            $table->tinyInteger('current_engagements')->unsigned()->default(0);
            $table->tinyInteger('ambitions_plan_of_action')->unsigned()->default(0);
            $table->tinyInteger('apra_career_advancement')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assessments');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubmissionAnswers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('submissions', function(Blueprint $table)
		{
            $table->string('proof_of_identity')->nullable()->default(null)->after('profile_image');
            $table->text('biography')->nullable()->default(null)->after('proof_of_identity');
            $table->text('achievements')->nullable()->default(null)->after('biography');
            $table->text('current_engagements')->nullable()->default(null)->after('achievements');
            $table->text('ambitions')->nullable()->default(null)->after('current_engagements');
            $table->text('plan_of_action')->nullable()->default(null)->after('ambitions');
            $table->text('apra_career_advancement')->nullable()->default(null)->after('plan_of_action');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('submissions', function(Blueprint $table)
		{
            $table->dropColumn([
                'biography',
                'achievements',
                'current_engagements',
                'ambitions',
                'plan_of_action',
                'apra_career_advancement',
            ]);
		});
	}

}

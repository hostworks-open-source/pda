<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddScoringColumnsToSubmissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('submissions', function (Blueprint $table) {
            $table->tinyInteger('partner_organisation_rank')->unsigned()->default(0)->after('progress_percent');
            $table->tinyInteger('round_one_rank')->unsigned()->default(0)->after('partner_organisation_rank');
            $table->tinyInteger('round_two_rank')->unsigned()->default(0)->after('round_one_rank');
            $table->boolean('round_one_shortlist')->default(false)->after('round_two_rank');
            $table->boolean('round_two_shortlist')->default(false)->after('round_one_shortlist');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('submissions', function (Blueprint $table) {
            $table->dropColumn([ 'partner_organisation_rank', 'round_one_rank', 'round_two_rank', 'round_one_shortlist', 'round_two_shortlist', ]);
        });
    }
}

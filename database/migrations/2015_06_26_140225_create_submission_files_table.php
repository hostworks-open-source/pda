<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmissionFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('submission_files', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('submission_id')->unsigned()->nullable()->default(null);
            $table->string('filename');
            $table->string('title')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->integer('filesize')->unsigned()->nullable()->default(null);
            $table->string('mime_type')->nullable()->default(null);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('submission_files');
	}

}

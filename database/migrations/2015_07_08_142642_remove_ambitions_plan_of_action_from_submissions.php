<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveAmbitionsPlanOfActionFromSubmissions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('submissions', function(Blueprint $table)
		{
			$table->dropColumn([ 'ambitions', 'plan_of_action', ]);
			$table->text('ambitions_plan_of_action')->after('current_engagements')->nullable()->default(null);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('submissions', function(Blueprint $table)
		{
			$table->dropColumn('ambitions_plan_of_action');
			$table->text('ambitions')->nullable()->default(null)->after('current_engagements');
			$table->text('plan_of_action')->nullable()->default(null)->after('ambitions');
		});
	}

}

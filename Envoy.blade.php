@servers([ 'remote' => '{user@host}', 'local' => '127.0.0.1', ])

@setup
if ( ! isset($user))
{
    throw new Exception('--user must be specified');
}

if ( ! isset($host))
{
    throw new Exception('--host must be specified');
}

if ( ! isset($repo))
{
    throw new Exception('--repo must be specified');
}

if ( ! isset($base_dir))
{
    throw new Exception('--base_dir must be specified');
}

$branch    = isset($branch) ? $branch : 'develop';
$repo_name = array_pop(explode('/', $repo));
$repo      = 'https://api.github.com/repos/' . $repo . '/tarball/' . $branch . '?access_token=' . getenv('GITHUB_TOKEN');
$env       = isset($env) ? $env : 'develop';
$release   = date('YmdHis');
$temp_dir  = '/tmp/pda';
@endsetup

@macro('deploy')
    prepare_env
    fetch_repo
    run_composer
    update_release
    down
    migrate
    up
    cleanup
@endmacro

@task('prepare_env', [ 'on' => 'remote', ])
    echo 'Configuring proxies';
    export http_proxy=http://0.0.0.0:8080;
    export HTTPS_PROXY=http://0.0.0.0:8080;

    if [ ! -e $HOME/composer.phar ]
    then
        echo 'Installing composer';
        curl -so $HOME/composer.phar https://getcomposer.org/composer.phar;
    fi
@endtask

@task('fetch_repo', [ 'on' => 'remote', ])
    [ -d {{ $temp_dir }} ] || mkdir {{ $temp_dir }};
    cd {{ $temp_dir }};

    mkdir {{ $release }};

    echo 'Fetching project tarball';
    curl -sLo {{ $release }}.tgz {{ $repo }};

    echo 'Extracting tarball';
    tar --strip-components=1 -zxf {{ $release }}.tgz -C {{ $release }};

    echo 'Purging tarball';
    rm -rf {{ $release }}.tgz;
@endtask

@task('run_composer', [ 'on' => 'remote', ])
    echo 'Installing composer dependencies';
    cd {{ $temp_dir }}/{{ $release }};
    php $HOME/composer.phar install --prefer-dist --no-scripts -q -o;
@endtask

@task('update_release', [ 'on' => 'remote', ])
    cd {{ $base_dir }};

    # Copy new release files
    echo 'Deploying new release code';
    rsync --exclude '/storage/' --exclude '/public/' --delete -a {{ $temp_dir }}/{{ $release }}/* ./;

    # Import the environment config
    echo 'Linking .env file';
    cp "$HOME/apra_pda/.env" {{ $base_dir }}/.env;

    # Optimise installation
    echo 'Optimising installation';
    php artisan clear-compiled;
    php artisan optimize;
    php artisan route:cache;
    php artisan config:cache;
@endtask

@task('migrate', [ 'on' => 'remote', ])
    echo 'Running migrations';
    cd {{ $base_dir }};
    php artisan migrate --force;
@endtask

@task('down', [ 'on' => 'remote', ])
    cd {{ $base_dir }};
    php artisan down;
@endtask

@task('up', [ 'on' => 'remote', ])
    cd {{ $base_dir }};
    php artisan up;
@endtask

@task('build_install', [ 'on' => 'local', ])
    echo 'Installing public asset dependencies';
    npm install
    bower install
@endtask

@task('build', [ 'on' => 'local', 'confirm' => true, ])
    echo 'Building and deploying public assets';
    gulp --production
    rsync -a --delete public/* {{ $user }}{{ '@' . $host }}:{{ $base_dir }}/public;
@endtask

@task('cleanup', [ 'on' => 'remote', ])
    echo 'Purging temporary files';
    rm -rf {{ $temp_dir }}/{{ $release }};
@endtask

@task('seed', [ 'on' => 'remote', ])
    cd {{ $base_dir }};
    php artisan db:seed --force;
@endtask

@task('db_refresh', [ 'on' => 'remote', 'confirm' => true, ])
    echo 'Re-running migrations and seeding database';
    cd {{ $base_dir }};
    php artisan migrate:refresh --seed --force;
@endtask

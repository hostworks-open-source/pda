var gulp = require('gulp');
var elixir = require('laravel-elixir');
var del = require('del');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.extend('remove', function (path) {
    gulp.task('remove', function () {
        del(path);
    });
    return this.queueTask('remove');
});

elixir(function (mix) {
    mix.less('app.less', 'public/css', {
        paths: __dirname + '/resources/vendor/bootstrap/less'
    })
        .scripts(['jquery/dist/jquery.min.js', 'bootstrap/dist/js/bootstrap.js', 'dropzone/dist/dropzone.js', 'rangeslider.js/dist/rangeslider.js'], 'public/js/app.js', 'resources/vendor/')
        .scripts(['html5shiv/dist/html5shiv.min.js', 'respondJs/dest/respond.min.js'], 'public/js/bc.js', 'resources/vendor/')
        .styles(['resources/vendor/dropzone/dist/dropzone.css', 'resources/vendor/fontawesome/css/font-awesome.css', 'resources/vendor/rangeslider.js/dist/rangeslider.css', 'public/css/app.css'], 'public/css/app.css', './')
        .version(['css/app.css', 'js/app.js', 'js/bc.js'])
        .copy('resources/vendor/bootstrap/fonts/**', 'public/build/fonts')
        .copy('resources/vendor/fontawesome/fonts/**', 'public/build/fonts')
        .remove(['public/css', 'public/js']);
});
